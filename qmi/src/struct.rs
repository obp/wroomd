use crate::error::{Error, Result};

pub trait QmiStruct: Sized {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])>;
    fn to_bytes(&self) -> Result<Vec<u8>>;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Unprefixed<T>(pub T);

impl QmiStruct for u8 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.is_empty() {
            Err(Error::Eof)
        } else {
            Ok((bytes[0], &bytes[1..]))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok(vec![*self])
    }
}

impl QmiStruct for u16 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.len() < 2 {
            Err(Error::Eof)
        } else {
            Ok(((bytes[1] as u16) << 8 | (bytes[0] as u16), &bytes[2..]))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok(vec![(self & 0xFF) as u8, (self >> 8) as u8])
    }
}

impl QmiStruct for u32 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.len() < 4 {
            Err(Error::Eof)
        } else {
            Ok((
                (bytes[3] as u32) << 24
                    | (bytes[2] as u32) << 16
                    | (bytes[1] as u32) << 8
                    | (bytes[0] as u32),
                &bytes[4..],
            ))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok(vec![
            (self & 0xFF) as u8,
            ((self >> 8) & 0xFF) as u8,
            ((self >> 16) & 0xFF) as u8,
            (self >> 24) as u8,
        ])
    }
}

impl QmiStruct for u64 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.len() < 8 {
            Err(Error::Eof)
        } else {
            Ok((
                (bytes[7] as u64) << 56
                    | (bytes[6] as u64) << 48
                    | (bytes[5] as u64) << 40
                    | (bytes[4] as u64) << 32
                    | (bytes[3] as u64) << 24
                    | (bytes[2] as u64) << 16
                    | (bytes[1] as u64) << 8
                    | (bytes[0] as u64),
                &bytes[8..],
            ))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok(vec![
            (self & 0xFF) as u8,
            ((self >> 8) & 0xFF) as u8,
            ((self >> 16) & 0xFF) as u8,
            ((self >> 24) & 0xFF) as u8,
            ((self >> 32) & 0xFF) as u8,
            ((self >> 40) & 0xFF) as u8,
            ((self >> 48) & 0xFF) as u8,
            (self >> 56) as u8,
        ])
    }
}

impl QmiStruct for i8 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.is_empty() {
            Err(Error::Eof)
        } else {
            Ok((bytes[0] as i8, &bytes[1..]))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok(vec![*self as u8])
    }
}

impl QmiStruct for i16 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        let (i, rest) = u16::from_bytes(bytes)?;
        Ok((i as i16, rest))
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        (*self as u16).to_bytes()
    }
}

impl QmiStruct for i32 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        let (i, rest) = u32::from_bytes(bytes)?;
        Ok((i as i32, rest))
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        (*self as u32).to_bytes()
    }
}

impl QmiStruct for i64 {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        let (i, rest) = u64::from_bytes(bytes)?;
        Ok((i as i64, rest))
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        (*self as u64).to_bytes()
    }
}

impl QmiStruct for Unprefixed<String> {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        Ok((Unprefixed(String::from_utf8_lossy(bytes).to_string()), &[]))
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        Ok((*self.0.clone().into_bytes()).to_vec())
    }
}

impl QmiStruct for String {
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.is_empty() {
            Err(Error::Eof)
        } else {
            let len = bytes[0] as usize;
            let rest = &bytes[1..];
            if rest.len() < len {
                Err(Error::Eof)
            } else {
                Ok((
                    String::from_utf8_lossy(&rest[..len]).to_string(),
                    &rest[len..],
                ))
            }
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        match u8::try_from(self.len()) {
            Ok(len) => Ok([vec![len], (*self.clone().into_bytes()).to_vec()].concat()),
            Err(_) => Err(Error::SequenceTooLong),
        }
    }
}

fn parse_vec<T>(bytes: &[u8], len: usize) -> Result<(Vec<T>, &[u8])>
where
    T: QmiStruct,
{
    let first = super::first_struct_from_bytes(bytes)?;

    let pairs: Vec<(T, _)> =
        std::iter::successors(Some(Ok(first)), |prev: &Result<(T, &[u8])>| match &prev {
            Ok((_, next)) => {
                if next.is_empty() {
                    None
                } else {
                    Some(super::first_struct_from_bytes(next))
                }
            }
            Err(_) => None,
        })
        .take(len)
        .collect::<Result<Vec<(T, _)>>>()?;

    let (values, rests): (Vec<_>, Vec<_>) = pairs.into_iter().unzip();

    let rest = rests.last().unwrap_or(&bytes);

    Ok((values, rest))
}

impl<T> QmiStruct for Unprefixed<Vec<T>>
where
    T: QmiStruct,
{
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.is_empty() {
            Ok((Unprefixed(vec![]), bytes))
        } else {
            parse_vec(bytes, usize::MAX).map(|(v, rest)| (Unprefixed(v), rest))
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        let bytes = self
            .0
            .iter()
            .map(|e| e.to_bytes())
            .collect::<Result<Vec<_>>>()?;
        Ok(bytes.concat())
    }
}

impl<T> QmiStruct for Vec<T>
where
    T: QmiStruct,
{
    fn from_bytes(bytes: &[u8]) -> Result<(Self, &[u8])> {
        if bytes.is_empty() {
            Err(Error::Eof)
        } else {
            let len = bytes[0] as usize;
            let rest = &bytes[1..];
            if len == 0 {
                Ok((vec![], rest))
            } else {
                parse_vec(rest, len)
            }
        }
    }
    fn to_bytes(&self) -> Result<Vec<u8>> {
        match u8::try_from(self.len()) {
            Ok(len) => {
                let elems: Vec<Vec<u8>> = self
                    .iter()
                    .map(|e| e.to_bytes())
                    .collect::<Result<Vec<_>>>()?;
                Ok([vec![vec![len]], elems].concat().concat())
            }
            Err(_) => Err(Error::SequenceTooLong),
        }
    }
}
