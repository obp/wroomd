use super::services::{dms, dpm, nas, uim, voice, wds, EpId, EpType, QmiService, Subscription};
use super::{Client, QmiDevice};
use crate::devices::util::{gsm8_decode, ucs2le_decode};
use crate::devices::{
    Action, ActionError, CallID, CallState, CallStatus, CardStatus, Code, CodeKind, DataConfigIpv4,
    DataConfigIpv6, DataNetworkID, DataNetworkState, DataNetworkStatus, DeviceID, Direction, Lock,
    NetworkState, NetworkStatus, NetworkTechnology, PortID, PortIndex, SlotID, SlotStatus,
};
use futures::stream::{self, StreamExt, TryStreamExt};
use ipnet::{Ipv4Net, Ipv6Net};
use log::{debug, info};
use qmi::{check_result, QmiMessage};
use std::collections::HashMap;
use std::net::{Ipv4Addr, Ipv6Addr};

type BoxDevice = Box<dyn QmiDevice + Sync + Send>;

async fn slot_from_client(ctx: &BoxDevice, cid: Client) -> Result<u8, ActionError> {
    let subs = match cid {
        Client::Subs(s) => s,
        _ => panic!("slot_from_client called on non-subscription client"),
    };
    ctx.state()
        .with(move |state| state.session_slots.get(subs as usize).copied())
        .await?
        .flatten()
        .ok_or(ActionError::NotMapped)
}

async fn slot_to_client(ctx: &BoxDevice, slot: u8) -> Result<Client, ActionError> {
    let index = ctx
        .state()
        .with(move |state| state.slot_sessions.get(slot as usize).copied())
        .await?
        .flatten()
        .ok_or(ActionError::NotMapped)?;
    Ok(Client::Subs(index))
}

async fn unwrap_network_client(
    ctx: &BoxDevice,
    dev: DeviceID,
    cid: Client,
) -> Result<(wds::IpFamily, DataNetworkID, u8), ActionError> {
    let (subs, family, net_id) = match cid {
        Client::Net(subs, net_id) => (subs, wds::IpFamily::Ipv4, net_id),
        Client::Net6(subs, net_id) => (subs, wds::IpFamily::Ipv6, net_id),
        _ => panic!("unwrap_network_client called on non-network client"),
    };
    let (slot, port) = ctx
        .state()
        .with(move |state| {
            let slot = state.session_slots.get(subs as usize).copied().flatten();
            let port = match state.ports.get(&(subs, net_id)) {
                Some(port) => *port,
                None => {
                    let port = state.last_used_port + 1;
                    state.ports.insert((subs, net_id), port);
                    state.last_used_port = port;
                    port
                }
            };
            (slot, port)
        })
        .await?;
    Ok((
        family,
        DataNetworkID {
            slot: SlotID {
                device: dev,
                slot: slot.ok_or(ActionError::NotMapped)?,
            },
            network: net_id,
        },
        port,
    ))
}

async fn process_response(
    ctx: &BoxDevice,
    txn_id: u16,
    result: Option<qmi::QmiResult>,
) -> Result<(), ActionError> {
    ctx.state()
        .complete(
            txn_id as u32,
            check_result(result.as_ref()).or_else(|e| match e {
                qmi::Error::NoEffect => Ok(()),
                _ => Err(e.into()),
            }),
        )
        .await
}

async fn process_dms_resp(
    ctx: BoxDevice,
    _dev: DeviceID,
    txn_id: u16,
    msg: dms::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        dms::Resp::SetOperatingMode { result } => process_response(&ctx, txn_id, result).await,
    }
}

async fn process_dpm_resp(
    ctx: BoxDevice,
    _dev: DeviceID,
    txn_id: u16,
    msg: dpm::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        dpm::Resp::OpenPort { result } => process_response(&ctx, txn_id, result).await,
    }
}

async fn process_nas_resp(
    ctx: BoxDevice,
    _dev: DeviceID,
    txn_id: u16,
    msg: nas::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        nas::Resp::RegisterIndications { result } => process_response(&ctx, txn_id, result).await,
        nas::Resp::BindSubscription { result } => process_response(&ctx, txn_id, result).await,
        nas::Resp::SetDualStandbyPreference { result } => {
            process_response(&ctx, txn_id, result).await
        }
        nas::Resp::ConfigurePLMNNameReporting { result } => {
            process_response(&ctx, txn_id, result).await
        }
    }
}

async fn process_nas_ind(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    let (_txn_id, msg) = nas::Ind::from_bytes(data)?;
    let slot = SlotID {
        device: dev,
        slot: slot_from_client(&ctx, cid).await?,
    };
    debug!("The message is an indication: {:#?}", msg);
    match msg {
        nas::Ind::ServingSystem {
            serving_system,
            roaming_indicator,
            data_service_cap,
            ..
        } => {
            let ss = serving_system.ok_or(ActionError::Other(String::from(
                "QMI ServingSystem indication missing serving system",
            )))?;
            use nas::AttachState as AS;
            use nas::DataCapability as DC;
            use nas::RegistrationState as RS;
            let new_state = match ss.cs_attach {
                AS::Unknown | AS::Detached => NetworkState::Unavailable,
                AS::Attached => match ss.registration {
                    RS::NotRegistered | RS::Unknown => NetworkState::Unavailable,
                    RS::Registered => NetworkState::Registered,
                    RS::Searching => NetworkState::Searching,
                    RS::Denied => NetworkState::Denied,
                },
            };
            ctx.state()
                .update_slot(slot, move |SlotStatus { card, network }| SlotStatus {
                    card: card.clone(),
                    network: NetworkStatus {
                        signal: network.signal,
                        state: new_state,
                        technology: match data_service_cap {
                            Some(caps) => match caps.get(0) {
                                Some(cap) => match cap {
                                    DC::GPRS => NetworkTechnology::GPRS,
                                    DC::EDGE => NetworkTechnology::EDGE,
                                    DC::HSDPA => NetworkTechnology::HSDPA,
                                    DC::HSUPA => NetworkTechnology::HSUPA,
                                    DC::WCDMA => NetworkTechnology::UMTS,
                                    DC::GSM => NetworkTechnology::GSM,
                                    DC::LTE => NetworkTechnology::LTE,
                                    DC::HSDPAPlus => NetworkTechnology::HSPAPlus,
                                    DC::DCHSDPAPlus => NetworkTechnology::HSPAPlus,
                                    _ => NetworkTechnology::None,
                                },
                                _ => NetworkTechnology::None,
                            },
                            _ => network.technology.clone(),
                        },
                        operator: network.operator.clone(),
                        roaming: match roaming_indicator {
                            Some(ind) => match ind {
                                nas::RoamingIndicator::On => true,
                                nas::RoamingIndicator::Off => false,
                            },
                            None => network.roaming,
                        },
                    },
                })
                .await
        }
        nas::Ind::CurrentPLMNName { spn, .. } => {
            let spn = spn.ok_or(ActionError::Other(String::from(
                "QMI PLMN name indication missing SPN",
            )))?;
            let name = match spn.encoding {
                nas::Encoding::GSM8 => gsm8_decode(&spn.name),
                nas::Encoding::UCS2LE => ucs2le_decode(&spn.name),
            };
            ctx.state()
                .update_slot(slot, move |SlotStatus { card, network }| SlotStatus {
                    card: card.clone(),
                    network: NetworkStatus {
                        signal: network.signal,
                        state: network.state.clone(),
                        technology: network.technology.clone(),
                        operator: name,
                        roaming: network.roaming,
                    },
                })
                .await
        }
    }
}

pub fn card_status_from_app(card: &uim::CardInfo, priority: u32, app: &uim::AppInfo) -> CardStatus {
    fn code(kind: CodeKind, info: &uim::PinInfo) -> Code {
        Code {
            kind,
            retries: info.retries,
            unblock_retries: info.puk_retries,
            verified: matches!(
                info.state,
                uim::PinState::Verified | uim::PinState::Disabled,
            ),
        }
    }
    use CodeKind::*;
    CardStatus {
        priority,
        lock: match app.state {
            uim::AppState::PinRequired => Lock::PIN,
            uim::AppState::PukRequired => Lock::PUK,
            uim::AppState::PersoRequired => Lock::Personalization,
            uim::AppState::PermanentlyBlocked => Lock::Permanent,
            _ => Lock::None,
        },
        codes: vec![
            match app.main_pin {
                uim::MainPinType::PIN1 => code(PIN1, &app.pin1),
                uim::MainPinType::UPIN => code(UPIN, &card.upin),
            },
            code(PIN2, &app.pin2),
        ],
    }
}

async fn process_card_status(
    ctx: &BoxDevice,
    dev: DeviceID,
    cs: uim::CardStatus,
) -> Result<(), ActionError> {
    // The card with the highest priority is the primary subscription,
    // so we have priorities ranging from 1 to the number of subscriptions
    // in reverse order.
    // A priority of 0 indicates that the card is not used.
    let priorities: HashMap<u8, u32> = cs
        .index_gw
        .iter()
        .zip((1..=(cs.index_gw.len() as u32)).rev())
        .map(|(sess, prio)| (sess.slot, prio))
        .collect();
    let nslots: u8 = cs.cards.len().try_into().expect("too many SIM slots");
    let session_slots: Vec<Option<u8>> = cs
        .index_gw
        .into_iter()
        .map(|sess| {
            if sess.slot < nslots {
                Some(sess.slot)
            } else {
                None
            }
        })
        .collect();
    let slot_sessions: Vec<Option<u8>> = (0..nslots)
        .map(|i| {
            session_slots
                .iter()
                .zip(0..)
                .find_map(|(slot, j)| if *slot == Some(i) { Some(j) } else { None })
        })
        .collect();
    let slot_updates = cs.cards.into_iter().zip(0..).map(|(card, id)| {
        let new_card_status = match card.state {
            uim::CardState::Present => card
                .applications
                .iter()
                .find(|app| matches!(app.ty, uim::AppType::SIM | uim::AppType::USIM))
                .map(|app| {
                    (
                        card_status_from_app(&card, *priorities.get(&id).unwrap_or(&0), app),
                        app.aid.clone(),
                    )
                }),
            _ => None,
        };
        let slot = SlotID {
            device: dev,
            slot: id,
        };
        match new_card_status {
            Some((card, aid)) => (slot, Some(card), Some(aid)),
            None => (slot, None, None),
        }
    });
    ctx.state()
        .with(move |state| {
            state.session_slots = session_slots;
            state.slot_sessions = slot_sessions;
        })
        .await?;
    stream::iter(slot_updates)
        .map(Ok)
        .try_for_each(|(slot, card, aid)| async move {
            let slot_id = slot.slot;
            ctx.state()
                .update_slot(slot, move |SlotStatus { network, .. }| SlotStatus {
                    card,
                    network: network.clone(),
                })
                .await?;
            ctx.state()
                .with(move |state| {
                    match aid {
                        Some(aid) => state.aids.insert(slot_id, aid),
                        None => state.aids.remove(&slot_id),
                    };
                })
                .await
        })
        .await?;
    Ok(())
}

async fn process_uim_resp(
    ctx: BoxDevice,
    dev: DeviceID,
    txn_id: u16,
    msg: uim::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        uim::Resp::RegisterEvents { result } => process_response(&ctx, txn_id, result).await,
        uim::Resp::GetCardStatus {
            result,
            card_status,
        } => {
            process_response(&ctx, txn_id, result).await?;
            match card_status {
                Some(cs) => process_card_status(&ctx, dev, cs).await,
                None => Err(ActionError::Other(String::from(
                    "No QMI extended card status found",
                ))),
            }
        }
        uim::Resp::VerifyPin { result, .. } => {
            process_response(&ctx, txn_id, result).await?;
            ctx.state().action(Action::QmiReloadCardStatus).await
        }
        uim::Resp::UnblockPin { result, .. } => {
            process_response(&ctx, txn_id, result).await?;
            ctx.state().action(Action::QmiReloadCardStatus).await
        }
        uim::Resp::ChangeProvisioningSession { result } => {
            process_response(&ctx, txn_id, result).await
        }
    }
}

async fn process_uim_ind(ctx: BoxDevice, dev: DeviceID, data: &[u8]) -> Result<(), ActionError> {
    let (_txn_id, msg) = uim::Ind::from_bytes(data)?;
    debug!("The message is an indication: {:#?}", msg);
    match msg {
        uim::Ind::CardStatus { card_status } => match card_status {
            Some(cs) => process_card_status(&ctx, dev, cs).await,
            None => Err(ActionError::Other(String::from(
                "No QMI extended card status found",
            ))),
        },
    }
}

async fn process_call_status(
    ctx: &BoxDevice,
    slot: &SlotID,
    ci: Vec<voice::CallInfo>,
    rpn: Option<Vec<voice::CallNumberInfo>>,
) -> Result<(), ActionError> {
    let numbers: HashMap<u8, String> = rpn
        .into_iter()
        .flatten()
        .map(|ni| (ni.id, ni.number))
        .collect();
    let call_updates = ci.into_iter().map(|info| {
        let direction = match info.dir {
            voice::CallDirection::Unknown => Direction::Unknown,
            voice::CallDirection::Outgoing => Direction::Outgoing,
            voice::CallDirection::Incoming => Direction::Incoming,
        };
        let state = match info.state {
            voice::CallState::Origination => CallState::Initializing,
            voice::CallState::Incoming => CallState::Ringing, // in
            voice::CallState::Conversation => CallState::Active,
            voice::CallState::CCInProgress => CallState::Initializing,
            voice::CallState::Alerting => CallState::Ringing, // out
            voice::CallState::Hold => CallState::Hold,
            voice::CallState::Waiting => CallState::Waiting,
            voice::CallState::Disconnecting => CallState::Ending,
            // voice::CallState::End is handled below by removing the call
            _ => CallState::Unknown,
        };
        let call = CallID {
            slot: slot.clone(),
            call: info.id,
        };
        match info.state {
            voice::CallState::Unknown | voice::CallState::End => (call, None),
            _ => (
                call,
                Some((numbers.get(&info.id).cloned(), direction, state)),
            ),
        }
    });
    stream::iter(call_updates)
        .map(Ok)
        .try_for_each(|(call, status)| async move {
            match status {
                Some((new_number, direction, state)) => {
                    ctx.state()
                        .update_call(call, move |CallStatus { number, .. }| CallStatus {
                            number: match new_number {
                                Some(n) => n,
                                None => number.clone(),
                            },
                            direction,
                            state,
                        })
                        .await
                }
                None => ctx.state().remove_call(call).await,
            }
        })
        .await?;
    Ok(())
}

async fn process_voice_resp(
    ctx: BoxDevice,
    _dev: DeviceID,
    txn_id: u16,
    msg: voice::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        voice::Resp::RegisterIndications { result } => process_response(&ctx, txn_id, result).await,
        voice::Resp::DialCall { result, .. } => process_response(&ctx, txn_id, result).await,
        voice::Resp::EndCall { result, .. } => process_response(&ctx, txn_id, result).await,
        voice::Resp::AnswerCall { result, .. } => process_response(&ctx, txn_id, result).await,
        voice::Resp::BindSubscription { result } => process_response(&ctx, txn_id, result).await,
    }
}

async fn process_voice_ind(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    let (_txn_id, msg) = voice::Ind::from_bytes(data)?;
    debug!("The message is an indication: {:#?}", msg);
    match msg {
        voice::Ind::AllCallStatus {
            call_info,
            remote_party_number,
        } => match call_info {
            Some(ci) => {
                process_call_status(
                    &ctx,
                    &SlotID {
                        device: dev,
                        slot: slot_from_client(&ctx, cid).await?,
                    },
                    ci,
                    remote_party_number,
                )
                .await
            }
            None => Err(ActionError::Other(String::from("No QMI call info found"))),
        },
    }
}

fn build_ipv4_addr(a: wds::Ipv4Addr) -> Ipv4Addr {
    Ipv4Addr::new(a._0, a._1, a._2, a._3)
}

fn build_ipv6_addr(a: wds::Ipv6Addr) -> Ipv6Addr {
    Ipv6Addr::new(
        (a._0 as u16) << 8 | (a._1 as u16),
        (a._2 as u16) << 8 | (a._3 as u16),
        (a._4 as u16) << 8 | (a._5 as u16),
        (a._6 as u16) << 8 | (a._7 as u16),
        (a._8 as u16) << 8 | (a._9 as u16),
        (a._a as u16) << 8 | (a._b as u16),
        (a._c as u16) << 8 | (a._d as u16),
        (a._e as u16) << 8 | (a._f as u16),
    )
}

fn build_ipv4_net(a: wds::Ipv4Addr, m: wds::Ipv4Addr) -> Option<Ipv4Net> {
    Ipv4Net::with_netmask(build_ipv4_addr(a), build_ipv4_addr(m)).ok()
}

fn build_ipv6_net(n: wds::Ipv6Net) -> Option<Ipv6Net> {
    Ipv6Net::new(build_ipv6_addr(n.addr), n.prefix_len).ok()
}

async fn process_wds_resp(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    txn_id: u16,
    msg: wds::Resp,
) -> Result<(), ActionError> {
    debug!("The message is a response: {:#?}", msg);
    match msg {
        wds::Resp::RegisterIndications { result } => process_response(&ctx, txn_id, result).await,
        wds::Resp::StartNetwork { result, .. } => process_response(&ctx, txn_id, result).await,
        wds::Resp::GetProfileList {
            result,
            profile_list,
            ..
        } => {
            process_response(&ctx, txn_id, result).await?;
            let subs = match cid {
                Client::Subs(s) => s,
                Client::Net(s, _) => s,
                Client::Net6(s, _) => s,
            };
            let profiles = profile_list.ok_or(ActionError::Other(String::from(
                "QMI WDS response missing profile list",
            )))?;
            stream::iter(profiles)
                .map(Ok)
                .try_for_each(|profile| {
                    ctx.state()
                        .action(Action::QmiReloadNetwork(subs, profile.id.index))
                })
                .await
        }
        wds::Resp::GetProfileSettings {
            result, apn_name, ..
        } => {
            process_response(&ctx, txn_id, result).await?;
            let (_, net_id, port) = unwrap_network_client(&ctx, dev, cid).await?;
            // This only works on platforms with bam-dmux.
            // TODO: support QMAP for use on newer platforms
            transaction(
                &ctx,
                Client::Subs(0), // the client doesn't matter here
                QmiService::DPM,
                dpm::Req::OpenPort {
                    control_ports: None,
                    hardware_data_ports: Some(vec![dpm::HardwareDataPort {
                        ep_id: EpId {
                            ty: EpType::BamDmux,
                            iface: port as u32,
                        },
                        rx_ep: port as u32,
                        tx_ep: port as u32,
                    }]),
                    software_data_ports: None,
                },
            )
            .await?;
            ctx.state()
                .update_net(net_id, |net| DataNetworkStatus {
                    apn: match apn_name {
                        Some(qmi::Unprefixed(name)) => name,
                        None => net.apn.clone(),
                    },
                    state_ipv4: net.state_ipv4.clone(),
                    state_ipv6: net.state_ipv6.clone(),
                    config_ipv4: net.config_ipv4.clone(),
                    config_ipv6: net.config_ipv6.clone(),
                })
                .await
        }
        wds::Resp::GetCurrentSettings {
            result,
            ip_addr,
            ip_gateway,
            ip_netmask,
            ipv6_addr,
            ipv6_gateway,
        } => {
            process_response(&ctx, txn_id, result).await?;
            let (net_id, ipv4, ipv6) = match unwrap_network_client(&ctx, dev, cid).await? {
                (wds::IpFamily::Ipv4, net, port) => (
                    net,
                    Some(DataConfigIpv4 {
                        ifname: None,
                        port: Some(PortID {
                            device: dev,
                            port: PortIndex::Mux(port),
                        }),
                        addr: match (ip_addr, ip_netmask) {
                            (Some(a), Some(m)) => build_ipv4_net(a, m),
                            _ => None,
                        },
                        gateway: ip_gateway.map(build_ipv4_addr),
                        dns: None,
                    }),
                    None,
                ),
                (wds::IpFamily::Ipv6, net, port) => (
                    net,
                    None,
                    Some(DataConfigIpv6 {
                        ifname: None,
                        port: Some(PortID {
                            device: dev,
                            port: PortIndex::Mux(port),
                        }),
                        addr: ipv6_addr.and_then(build_ipv6_net),
                        gateway: ipv6_gateway.map(|n| n.addr).map(build_ipv6_addr),
                        dns: None,
                    }),
                ),
            };
            ctx.state()
                .update_net(net_id, |net| DataNetworkStatus {
                    apn: net.apn.clone(),
                    state_ipv4: net.state_ipv4.clone(),
                    state_ipv6: net.state_ipv6.clone(),
                    config_ipv4: match ipv4 {
                        Some(new_conf) => new_conf,
                        None => net.config_ipv4.clone(),
                    },
                    config_ipv6: match ipv6 {
                        Some(new_conf) => new_conf,
                        None => net.config_ipv6.clone(),
                    },
                })
                .await
        }
        wds::Resp::SetIpFamily { result } => process_response(&ctx, txn_id, result).await,
        wds::Resp::BindMuxDataPort { result } => process_response(&ctx, txn_id, result).await,
        wds::Resp::BindSubscription { result } => process_response(&ctx, txn_id, result).await,
    }
}

fn network_state_from_status(st: wds::ConnectionStatusUpdate) -> DataNetworkState {
    use wds::ConnectionStatus::*;
    match st.status {
        Unknown | Disconnected => DataNetworkState::Idle,
        Connected => DataNetworkState::Active,
        Suspended => DataNetworkState::Paused,
        Authenticating => DataNetworkState::Configuring,
    }
}

async fn process_wds_ind(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    let (_txn_id, msg) = wds::Ind::from_bytes(data)?;
    debug!("The message is an indication: {:#?}", msg);
    if let wds::Ind::PacketServiceStatus {
        status: Some(st), ..
    } = msg
    {
        match unwrap_network_client(&ctx, dev, cid).await? {
            (wds::IpFamily::Ipv4, id, _) => {
                ctx.state()
                    .update_net(id, move |net| DataNetworkStatus {
                        state_ipv4: network_state_from_status(st),
                        ..net.clone()
                    })
                    .await?
            }
            (wds::IpFamily::Ipv6, id, _) => {
                ctx.state()
                    .update_net(id, move |net| DataNetworkStatus {
                        state_ipv6: network_state_from_status(st),
                        ..net.clone()
                    })
                    .await?
            }
        }
    }
    match cid {
        Client::Net(_, _) | Client::Net6(_, _) => {
            ctx.state().action(Action::QmiReloadIpConfig(cid)).await
        }
        _ => Ok(()),
    }
}

async fn process_dms_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    _cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match dms::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_dms_resp(ctx, dev, txn_id, resp).await,
        // Err(qmi::Error::UnexpectedType) => process_dms_ind(ctx, dev, data).await,
        Err(err) => Err(err.into()),
    }
}

async fn process_dpm_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    _cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match dpm::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_dpm_resp(ctx, dev, txn_id, resp).await,
        // Err(qmi::Error::UnexpectedType) => process_dpm_ind(ctx, dev, data).await,
        Err(err) => Err(err.into()),
    }
}

async fn process_nas_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match nas::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_nas_resp(ctx, dev, txn_id, resp).await,
        Err(qmi::Error::UnexpectedType) => process_nas_ind(ctx, dev, cid, data).await,
        Err(err) => Err(err.into()),
    }
}

async fn process_uim_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    _cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match uim::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_uim_resp(ctx, dev, txn_id, resp).await,
        Err(qmi::Error::UnexpectedType) => process_uim_ind(ctx, dev, data).await,
        Err(err) => Err(err.into()),
    }
}

async fn process_voice_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match voice::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_voice_resp(ctx, dev, txn_id, resp).await,
        Err(qmi::Error::UnexpectedType) => process_voice_ind(ctx, dev, cid, data).await,
        Err(err) => Err(err.into()),
    }
}

async fn process_wds_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    data: &[u8],
) -> Result<(), ActionError> {
    match wds::Resp::from_bytes(data) {
        Ok((txn_id, resp)) => process_wds_resp(ctx, dev, cid, txn_id, resp).await,
        Err(qmi::Error::UnexpectedType) => process_wds_ind(ctx, dev, cid, data).await,
        Err(err) => Err(err.into()),
    }
}

pub(super) async fn process_msg(
    ctx: BoxDevice,
    dev: DeviceID,
    cid: Client,
    service: QmiService,
    data: &[u8],
) -> Result<(), ActionError> {
    debug!("{:?} message on {:?}, client {:?}", service, dev, cid);
    match service {
        QmiService::DMS => process_dms_msg(ctx, dev, cid, data).await,
        QmiService::DPM => process_dpm_msg(ctx, dev, cid, data).await,
        QmiService::NAS => process_nas_msg(ctx, dev, cid, data).await,
        QmiService::UIM => process_uim_msg(ctx, dev, cid, data).await,
        QmiService::Voice => process_voice_msg(ctx, dev, cid, data).await,
        QmiService::WDS => process_wds_msg(ctx, dev, cid, data).await,
        _ => Ok(()),
    }
}

fn session_from_slot(slot: u8) -> Result<uim::SessionType, qmi::Error> {
    match slot {
        0 => Ok(uim::SessionType::CardSlot1),
        1 => Ok(uim::SessionType::CardSlot2),
        2 => Ok(uim::SessionType::CardSlot3),
        3 => Ok(uim::SessionType::CardSlot4),
        4 => Ok(uim::SessionType::CardSlot5),
        _ => Err(qmi::Error::Message(String::from("Slot index out of range"))),
    }
}

fn provisioning_session_from_index(i: u8) -> Result<uim::SessionType, qmi::Error> {
    match i {
        0 => Ok(uim::SessionType::PrimaryGwProvisioning),
        1 => Ok(uim::SessionType::SecondaryGwProvisioning),
        2 => Ok(uim::SessionType::TertiaryGwProvisioning),
        3 => Ok(uim::SessionType::QuaternaryGwProvisioning),
        4 => Ok(uim::SessionType::QuinaryGwProvisioning),
        _ => Err(qmi::Error::Message(String::from(
            "Session index out of range",
        ))),
    }
}

fn unlock_card(slot: u8, code_kind: &CodeKind, code: &str) -> Result<uim::Req, qmi::Error> {
    Ok(uim::Req::VerifyPin {
        session: Some(uim::SessionInfo {
            session: session_from_slot(slot)?,
            aid: vec![], // does not matter here, we're operating at card level
        }),
        pin: Some(uim::VerifyPinInfo {
            id: match code_kind {
                CodeKind::PIN1 => uim::PinId::PIN1,
                CodeKind::PIN2 => uim::PinId::PIN2,
                CodeKind::UPIN => uim::PinId::UPIN,
            },
            value: code.to_string(),
        }),
    })
}

fn unblock_card(
    slot: u8,
    code_kind: &CodeKind,
    puk: &str,
    new: &str,
) -> Result<uim::Req, qmi::Error> {
    Ok(uim::Req::UnblockPin {
        session: Some(uim::SessionInfo {
            session: session_from_slot(slot)?,
            aid: vec![], // does not matter here, we're operating at card level
        }),
        pin: Some(uim::UnblockPinInfo {
            id: match code_kind {
                CodeKind::PIN1 => uim::PinId::PIN1,
                CodeKind::PIN2 => uim::PinId::PIN2,
                CodeKind::UPIN => uim::PinId::UPIN,
            },
            puk: puk.to_string(),
            new_pin: new.to_string(),
        }),
    })
}

fn change_provisioning_session(index: u8, slot: u8, aid: &[u8]) -> Result<uim::Req, qmi::Error> {
    Ok(uim::Req::ChangeProvisioningSession {
        session_change: Some(uim::SessionChange {
            session: provisioning_session_from_index(index)?,
            activate: 1,
        }),
        application: Some(uim::AppProvisioningInfo {
            slot: slot + 1,
            aid: aid.to_vec(),
        }),
    })
}

fn deactivate_provisioning_session(index: u8) -> Result<uim::Req, qmi::Error> {
    Ok(uim::Req::ChangeProvisioningSession {
        session_change: Some(uim::SessionChange {
            session: provisioning_session_from_index(index)?,
            activate: 0,
        }),
        application: None,
    })
}

async fn transaction(
    ctx: &BoxDevice,
    cid: Client,
    service: QmiService,
    msg: impl QmiMessage + Send + std::fmt::Debug + 'static,
) -> Result<(), ActionError> {
    debug!("({:?}) request: {:#?}", cid, msg);
    let txn_id = ctx
        .state()
        .with(|state| {
            let txn_id = state.txn_id;
            state.txn_id += 1;
            txn_id.0
        })
        .await?;
    ctx.send_msg(cid, service, msg.to_bytes(txn_id)?).await?;
    ctx.state().wait(txn_id as u32).await
}

async fn start_network(ctx: &BoxDevice, cid: Client) -> Result<(), ActionError> {
    let (subs_index, family, net_id) = match cid {
        Client::Net(subs, net_id) => (subs, wds::IpFamily::Ipv4, net_id),
        Client::Net6(subs, net_id) => (subs, wds::IpFamily::Ipv6, net_id),
        _ => panic!("network request not performed by network client"),
    };
    let subs = match subs_index {
        0 => Ok(wds::Subscription::Primary),
        1 => Ok(wds::Subscription::Secondary),
        2 => Ok(wds::Subscription::Tertiary),
        _ => Err(ActionError::NotMapped),
    }?;
    let port = ctx
        .state()
        .with(move |state| state.ports.get(&(subs_index, net_id)).copied())
        .await?
        .ok_or(ActionError::NotMapped)?;
    transaction(
        ctx,
        cid,
        QmiService::WDS,
        wds::Req::RegisterIndications {
            ext_ip_config_change: Some(1),
            profile_change: Some(0),
        },
    )
    .await?;
    // This only works on platforms with bam-dmux.
    // TODO: support QMAP for use on newer platforms
    transaction(
        ctx,
        cid,
        QmiService::WDS,
        wds::Req::BindMuxDataPort {
            ep_id: Some(EpId {
                ty: EpType::BamDmux,
                iface: port as u32,
            }),
            mux_id: Some(0),
        },
    )
    .await?;
    transaction(
        ctx,
        cid,
        QmiService::WDS,
        wds::Req::SetIpFamily {
            ip_family: Some(family),
        },
    )
    .await?;
    transaction(
        ctx,
        cid,
        QmiService::WDS,
        wds::Req::BindSubscription {
            subscription: Some(subs),
        },
    )
    .await?;
    transaction(
        ctx,
        cid,
        QmiService::WDS,
        wds::Req::StartNetwork {
            profile_index_3gpp: Some(net_id),
        },
    )
    .await
}

pub(super) async fn action(ctx: BoxDevice, _dev: DeviceID, act: Action) -> Result<(), ActionError> {
    let ctx = &ctx;
    match act {
        Action::QmiInitService(service) => match service {
            QmiService::DMS => {
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::DMS,
                    dms::Req::SetOperatingMode {
                        mode: Some(dms::OperatingMode::Online),
                    },
                )
                .await
            }
            QmiService::NAS => {
                let msg = nas::Req::RegisterIndications {
                    system_selection_preference: Some(0),
                    ddtm_events: Some(0),
                    serving_system_events: Some(1),
                    dual_standby_preference: Some(0),
                    subscription_info: Some(0),
                    network_time: Some(0),
                    system_info: Some(0),
                    signal_info: Some(0),
                    error_rate: Some(0),
                    hdr_new_uati_assigned: Some(0),
                    hdr_session_closed: Some(0),
                    managed_roaming: Some(0),
                    current_plmn_name: Some(1),
                    embms_status: Some(0),
                    rf_band_info: Some(0),
                    network_reject_info: None,
                };
                // The order here is important.
                // 1. Initialize client 1.
                let msg2 = msg.clone();
                transaction(ctx, Client::Subs(0), QmiService::NAS, msg).await?;

                // 2. Try binding the primary subscription.
                //    If this fails, we use the primary client as-is.
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::NAS,
                    nas::Req::BindSubscription {
                        subscription: Some(Subscription::Primary),
                    },
                )
                .await?;

                // 3. Try binding the secondary subscription to client 2.
                //    If this fails, the secondary client will **not** be used.
                match transaction(
                    ctx,
                    Client::Subs(1),
                    QmiService::NAS,
                    nas::Req::BindSubscription {
                        subscription: Some(Subscription::Secondary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(1)).await;
                        info!("Secondary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                // 4. If this succeeds, initialize client 2.
                let msg3 = msg2.clone();
                transaction(ctx, Client::Subs(1), QmiService::NAS, msg2).await?;

                // 5. Do the same with client 3 and the tertiary subscription.
                //    If this fails, the tertiary client will **not** be used.
                match transaction(
                    ctx,
                    Client::Subs(2),
                    QmiService::NAS,
                    nas::Req::BindSubscription {
                        subscription: Some(Subscription::Tertiary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(2)).await;
                        info!("Tertiary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                transaction(ctx, Client::Subs(2), QmiService::NAS, msg3).await
            }
            QmiService::UIM => {
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::UIM,
                    uim::Req::GetCardStatus { extended: Some(1) },
                )
                .await?;
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::UIM,
                    uim::Req::RegisterEvents {
                        mask: Some(uim::EVENT_EXTENDED_CARD_STATUS),
                    },
                )
                .await
            }
            QmiService::Voice => {
                let msg = voice::Req::RegisterIndications {
                    dtmf_events: Some(0),
                    voice_privacy_events: Some(0),
                    sups_notifications: Some(0),
                    call_notifications: Some(1),
                    handover_events: Some(0),
                    speech_codec_events: Some(0),
                    ussd_notifications: Some(0),
                    modification_events: Some(0),
                    uus_events: Some(0),
                    aoc_events: Some(0),
                    conference_events: Some(0),
                    ext_burst_ii_events: Some(0),
                    mt_page_miss_events: Some(0),
                };
                // See comments above for NAS.
                let msg2 = msg.clone();
                transaction(ctx, Client::Subs(0), QmiService::Voice, msg).await?;
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::Voice,
                    voice::Req::BindSubscription {
                        subscription: Some(Subscription::Primary),
                    },
                )
                .await?;
                match transaction(
                    ctx,
                    Client::Subs(1),
                    QmiService::Voice,
                    voice::Req::BindSubscription {
                        subscription: Some(Subscription::Secondary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(1)).await;
                        info!("Secondary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                let msg3 = msg2.clone();
                transaction(ctx, Client::Subs(1), QmiService::Voice, msg2).await?;
                match transaction(
                    ctx,
                    Client::Subs(2),
                    QmiService::Voice,
                    voice::Req::BindSubscription {
                        subscription: Some(Subscription::Tertiary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(2)).await;
                        info!("Tertiary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                transaction(ctx, Client::Subs(2), QmiService::Voice, msg3).await
            }
            QmiService::WDS => {
                let msg = wds::Req::GetProfileList {
                    profile_type: Some(wds::ProfileType::_3GPP),
                };
                // See comments above for NAS.
                let msg2 = msg.clone();
                transaction(ctx, Client::Subs(0), QmiService::WDS, msg).await?;
                transaction(
                    ctx,
                    Client::Subs(0),
                    QmiService::WDS,
                    wds::Req::BindSubscription {
                        subscription: Some(wds::Subscription::Primary),
                    },
                )
                .await?;
                match transaction(
                    ctx,
                    Client::Subs(1),
                    QmiService::WDS,
                    wds::Req::BindSubscription {
                        subscription: Some(wds::Subscription::Secondary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(1)).await;
                        info!("Secondary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                let msg3 = msg2.clone();
                transaction(ctx, Client::Subs(1), QmiService::WDS, msg2).await?;
                match transaction(
                    ctx,
                    Client::Subs(2),
                    QmiService::WDS,
                    wds::Req::BindSubscription {
                        subscription: Some(wds::Subscription::Tertiary),
                    },
                )
                .await
                {
                    Ok(_) => (),
                    Err(e) => {
                        ctx.close(Client::Subs(2)).await;
                        info!("Tertiary subscription unsupported: {e}");
                        return Ok(());
                    }
                }
                transaction(ctx, Client::Subs(2), QmiService::WDS, msg3).await
            }
            _ => Ok(()),
        },
        Action::QmiReloadCardStatus => {
            transaction(
                ctx,
                Client::Subs(0),
                QmiService::UIM,
                uim::Req::GetCardStatus { extended: Some(1) },
            )
            .await
        }
        Action::QmiReloadNetwork(subs, net_id) => {
            let cid = Client::Net(subs, net_id);
            transaction(
                ctx,
                cid,
                QmiService::WDS,
                wds::Req::GetProfileSettings {
                    id: Some(wds::ProfileId {
                        ty: wds::ProfileType::_3GPP,
                        index: net_id,
                    }),
                },
            )
            .await
        }
        Action::QmiReloadIpConfig(cid) => {
            transaction(
                ctx,
                cid,
                QmiService::WDS,
                wds::Req::GetCurrentSettings {
                    requested_settings: Some(wds::REQUEST_IP_ADDRESS | wds::REQUEST_GATEWAY_INFO),
                },
            )
            .await
        }
        Action::ActivateCard(this_slot) => {
            let this_slot_id = this_slot.slot;
            let updates: Vec<(u8, u8, Vec<u8>)> = ctx
                .state()
                .with(move |state| {
                    let nsessions: u8 = state
                        .session_slots
                        .len()
                        .try_into()
                        .expect("too many QMI sessions");
                    state
                        .session_slots
                        .iter()
                        .flatten()
                        .filter(|slot_id| **slot_id != this_slot_id)
                        .zip(1..nsessions)
                        .chain([(&this_slot_id, 0)])
                        .filter_map(|(slot_id, index)| {
                            state
                                .aids
                                .get(slot_id)
                                .map(|aid| (index, *slot_id, aid.to_vec()))
                        })
                        .collect()
                })
                .await?;
            stream::iter(updates)
                .map(Ok)
                .try_for_each(|(index, slot_id, aid)| async move {
                    transaction(
                        ctx,
                        Client::Subs(0),
                        QmiService::UIM,
                        change_provisioning_session(index, slot_id, &aid)?,
                    )
                    .await
                })
                .await
        }
        Action::DeactivateCard(slot) => {
            let slot_id = slot.slot;
            let index = ctx
                .state()
                .with(move |state| state.slot_sessions.get(slot_id as usize).copied())
                .await?
                .flatten();
            match index {
                Some(index) => {
                    transaction(
                        ctx,
                        Client::Subs(0),
                        QmiService::UIM,
                        deactivate_provisioning_session(index)?,
                    )
                    .await
                }
                None => Ok(()),
            }
        }
        Action::UnlockCard {
            slot,
            code_kind,
            code,
        } => {
            transaction(
                ctx,
                Client::Subs(0),
                QmiService::UIM,
                unlock_card(slot.slot, &code_kind, &code)?,
            )
            .await
        }
        Action::UnblockCard {
            slot,
            code_kind,
            puk,
            new_code,
        } => {
            transaction(
                ctx,
                Client::Subs(0),
                QmiService::UIM,
                unblock_card(slot.slot, &code_kind, &puk, &new_code)?,
            )
            .await
        }
        Action::Dial { slot, number } => {
            transaction(
                ctx,
                slot_to_client(ctx, slot.slot).await?,
                QmiService::Voice,
                voice::Req::DialCall {
                    number: Some(qmi::Unprefixed(number)),
                    ty: Some(voice::CallType::Voice),
                },
            )
            .await
        }
        Action::Accept(call) => {
            let call_id: u8 = call.call;
            transaction(
                ctx,
                slot_to_client(ctx, call.slot.slot).await?,
                QmiService::Voice,
                voice::Req::AnswerCall { id: Some(call_id) },
            )
            .await
        }
        Action::Hangup(call) => {
            let call_id: u8 = call.call;
            transaction(
                ctx,
                slot_to_client(ctx, call.slot.slot).await?,
                QmiService::Voice,
                voice::Req::EndCall { id: Some(call_id) },
            )
            .await
        }
        Action::StartNetwork(net) => {
            let net_id: u8 = net.network;
            let subs = ctx
                .state()
                .with(move |state| state.slot_sessions.get(net.slot.slot as usize).copied())
                .await?
                .flatten()
                .ok_or(ActionError::NotMapped)?;
            let bind_subs = match subs {
                0 => Ok(Subscription::Primary),
                1 => Ok(Subscription::Secondary),
                2 => Ok(Subscription::Tertiary),
                _ => Err(ActionError::NotMapped),
            }?;
            transaction(
                ctx,
                Client::Subs(0),
                QmiService::NAS,
                nas::Req::SetDualStandbyPreference {
                    data_subscription: Some(bind_subs),
                },
            )
            .await?;
            let (r1, r2) = tokio::join!(
                start_network(ctx, Client::Net(subs, net_id)),
                start_network(ctx, Client::Net6(subs, net_id)),
            );
            match &r1 {
                Ok(_) => (),
                Err(e) => info!("IPv4 network unavailable: {e}"),
            }
            match &r2 {
                Ok(_) => (),
                Err(e) => info!("IPv6 network unavailable: {e}"),
            }
            r1.or(r2) // one IP family is enough for a successful connection
        }
        Action::StopNetwork(net) => {
            let net_id: u8 = net.network;
            let subs = ctx
                .state()
                .with(move |state| state.slot_sessions.get(net.slot.slot as usize).copied())
                .await?
                .flatten()
                .ok_or(ActionError::NotMapped)?;
            ctx.close(Client::Net(subs, net_id)).await;
            ctx.close(Client::Net6(subs, net_id)).await;
            Ok(())
        }
        _ => {
            debug!("action not implemented: {:?}", act);
            Ok(())
        }
    }
}
