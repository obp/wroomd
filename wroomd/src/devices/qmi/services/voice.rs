use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct, Unprefixed};

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CallState {
    Unknown = 0,
    Origination = 1,
    Incoming = 2,
    Conversation = 3,
    CCInProgress = 4,
    Alerting = 5,
    Hold = 6,
    Waiting = 7,
    Disconnecting = 8,
    End = 9,
    Setup = 10,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CallType {
    Voice = 0,
    VoiceForced = 1,
    VoiceIP = 2,
    Video = 3,
    VideoShare = 4,
    Test = 5,
    OTAPA = 6,
    StandardOTASP = 7,
    NonStandardOTASP = 8,
    Emergency = 9,
    SupplementaryService = 10,
    EmergencyIP = 11,
    ECall = 12,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CallDirection {
    Unknown = 0,
    Outgoing = 1,
    Incoming = 2,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CallMode {
    None = 0,
    CDMA = 1,
    GSM = 2,
    UMTS = 3,
    LTE = 4,
    Unknown = 6,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum ALS {
    Line1 = 0,
    Line2 = 1,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct CallInfo {
    pub id: u8,
    pub state: CallState,
    pub ty: CallType,
    pub dir: CallDirection,
    pub mode: CallMode,
    pub multipart: u8,
    pub als: ALS,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct CallNumberInfo {
    pub id: u8,
    pub presentation: u8,
    pub number: String,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x10]
        dtmf_events: Option<u8>,
        #[id = 0x11]
        voice_privacy_events: Option<u8>,
        #[id = 0x12]
        sups_notifications: Option<u8>,
        #[id = 0x13]
        call_notifications: Option<u8>,
        #[id = 0x14]
        handover_events: Option<u8>,
        #[id = 0x15]
        speech_codec_events: Option<u8>,
        #[id = 0x16]
        ussd_notifications: Option<u8>,
        #[id = 0x18]
        modification_events: Option<u8>,
        #[id = 0x19]
        uus_events: Option<u8>,
        #[id = 0x1A]
        aoc_events: Option<u8>,
        #[id = 0x1B]
        conference_events: Option<u8>,
        #[id = 0x1C]
        ext_burst_ii_events: Option<u8>,
        #[id = 0x1D]
        mt_page_miss_events: Option<u8>,
    },
    #[id = 0x20]
    DialCall {
        #[id = 0x01]
        number: Option<Unprefixed<String>>,
        #[id = 0x10]
        ty: Option<CallType>,
    },
    #[id = 0x21]
    EndCall {
        #[id = 0x01]
        id: Option<u8>,
    },
    #[id = 0x22]
    AnswerCall {
        #[id = 0x01]
        id: Option<u8>,
    },
    #[id = 0x44]
    BindSubscription {
        #[id = 0x01]
        subscription: Option<super::Subscription>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x20]
    DialCall {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x10]
        call_id: Option<u8>,
    },
    #[id = 0x21]
    EndCall {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x10]
        call_id: Option<u8>,
    },
    #[id = 0x22]
    AnswerCall {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x10]
        call_id: Option<u8>,
    },
    #[id = 0x44]
    BindSubscription {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_indication]
pub enum Ind {
    #[id = 0x2E]
    AllCallStatus {
        #[id = 0x01]
        call_info: Option<Vec<CallInfo>>,
        #[id = 0x10]
        remote_party_number: Option<Vec<CallNumberInfo>>,
    },
}
