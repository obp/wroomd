use crate::devices::{
    Action, ActionError, CallID, CallStatus, DataNetworkID, DataNetworkStatus, PortID,
    SlotID, SlotStatus,
};
use futures::channel::{mpsc, oneshot};
use futures::stream::StreamExt;
use log::{debug, error, trace};
use std::collections::HashMap;
use std::marker::PhantomData;

pub type ResultSender = futures::channel::oneshot::Sender<Result<(), ActionError>>;

pub enum Event {
    UpdateSlot(
        SlotID,
        Box<dyn FnOnce(&SlotStatus) -> SlotStatus + Send>,
        ResultSender,
    ),
    UpdateCall(
        CallID,
        Box<dyn FnOnce(&CallStatus) -> CallStatus + Send>,
        ResultSender,
    ),
    RemoveCall(CallID, ResultSender),
    UpdateNet(
        DataNetworkID,
        Box<dyn FnOnce(&DataNetworkStatus) -> DataNetworkStatus + Send>,
        ResultSender,
    ),
    RemoveNet(DataNetworkID, ResultSender),
    NetPort(
        PortID,
        String,
    ),
}

pub enum Message<S> {
    Wait(u32, ResultSender),
    Complete(u32, Result<(), ActionError>),
    Access(Box<dyn Visitor<S> + Send>),
    Stop,
}

pub trait Visitor<S> {
    fn visit(self: Box<Self>, state: &mut S) -> Result<(), ActionError>;
}

pub struct StateVisitor<S, T, H>
where
    H: FnOnce(&mut S) -> T,
{
    sender: oneshot::Sender<Result<T, ActionError>>,
    handler: H,
    phantom: PhantomData<S>,
}

impl<S, T, H> Visitor<S> for StateVisitor<S, T, H>
where
    H: FnOnce(&mut S) -> T,
{
    fn visit(self: Box<Self>, state: &mut S) -> Result<(), ActionError> {
        let response = (self.handler)(state);
        self.sender
            .send(Ok(response))
            .map_err(|_| ActionError::Other(String::from("failed to send handler response")))
    }
}

pub struct StateContext<S> {
    sender: mpsc::UnboundedSender<Message<S>>,
    action_sender: mpsc::UnboundedSender<(Action, Option<ResultSender>)>,
    event_sender: mpsc::UnboundedSender<Event>,
}

impl<S> Clone for StateContext<S> {
    fn clone(&self) -> Self {
        StateContext {
            sender: self.sender.clone(),
            action_sender: self.action_sender.clone(),
            event_sender: self.event_sender.clone(),
        }
    }
}

impl<S: Send + 'static> StateContext<S> {
    pub async fn action(&self, action: Action) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .action_sender
            .clone()
            .unbounded_send((action, Some(tx)));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for action failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!("failed to send action: {e}"))),
        }
    }

    pub async fn wait(&self, token: u32) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self.sender.clone().unbounded_send(Message::Wait(token, tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for completion failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send completion token: {e}"
            ))),
        }
    }

    pub async fn complete(
        &self,
        token: u32,
        result: Result<(), ActionError>,
    ) -> Result<(), ActionError> {
        self.sender
            .clone()
            .unbounded_send(Message::Complete(token, result))
            .map_err(|e| ActionError::Other(format!("failed to send completion: {e}")))
    }

    pub async fn with<T>(
        &self,
        handler: impl FnOnce(&mut S) -> T + Send + 'static,
    ) -> Result<T, ActionError>
    where
        T: Send + 'static,
    {
        let (tx, rx) = oneshot::channel::<Result<T, ActionError>>();
        let result = self
            .sender
            .clone()
            .unbounded_send(Message::Access(Box::new(StateVisitor {
                sender: tx,
                handler,
                phantom: PhantomData,
            })));
        match result {
            Ok(_) => rx
                .await
                .map_err(|e| ActionError::Other(format!("state access returned error: {e}")))
                .and_then(|r| r),
            Err(e) => Err(ActionError::Other(format!("failed to send handler: {e}"))),
        }
    }

    pub async fn update_slot(
        &self,
        id: SlotID,
        handler: impl FnOnce(&SlotStatus) -> SlotStatus + Send + 'static,
    ) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .event_sender
            .clone()
            .unbounded_send(Event::UpdateSlot(id, Box::new(handler), tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for slot update failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send slot update: {e}"
            ))),
        }
    }

    pub async fn update_call(
        &self,
        id: CallID,
        handler: impl FnOnce(&CallStatus) -> CallStatus + Send + 'static,
    ) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .event_sender
            .clone()
            .unbounded_send(Event::UpdateCall(id, Box::new(handler), tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for call update failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send call update: {e}"
            ))),
        }
    }

    pub async fn remove_call(&self, id: CallID) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .event_sender
            .clone()
            .unbounded_send(Event::RemoveCall(id, tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for call removal failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send call removal: {e}"
            ))),
        }
    }

    pub async fn update_net(
        &self,
        id: DataNetworkID,
        handler: impl FnOnce(&DataNetworkStatus) -> DataNetworkStatus + Send + 'static,
    ) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .event_sender
            .clone()
            .unbounded_send(Event::UpdateNet(id, Box::new(handler), tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for network update failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send network update: {e}"
            ))),
        }
    }

    pub async fn remove_net(&self, id: DataNetworkID) -> Result<(), ActionError> {
        let (tx, rx) = oneshot::channel::<Result<(), ActionError>>();
        let result = self
            .event_sender
            .clone()
            .unbounded_send(Event::RemoveNet(id, tx));
        match result {
            Ok(_) => match rx.await {
                Ok(r) => r,
                Err(e) => Err(ActionError::Other(format!(
                    "waiting for network removal failed: {e}"
                ))),
            },
            Err(e) => Err(ActionError::Other(format!(
                "failed to send network removal: {e}"
            ))),
        }
    }
}

pub fn spawn_context<S: Send + 'static>(
    mut state: S,
    action_sender: mpsc::UnboundedSender<(Action, Option<ResultSender>)>,
    event_sender: mpsc::UnboundedSender<Event>,
) -> StateContext<S> {
    let (sender, receiver) = mpsc::unbounded::<Message<S>>();
    let ctx = StateContext {
        sender,
        action_sender,
        event_sender,
    };
    tokio::spawn(async move {
        let mut stream = Box::pin(receiver);
        let mut waiting = HashMap::<u32, ResultSender>::new();
        while let Some(ev) = stream.next().await {
            match ev {
                Message::Wait(token, tx) => {
                    debug!("waiting for {token}");
                    waiting.insert(token, tx);
                }
                Message::Complete(token, result) => match waiting.remove(&token) {
                    Some(tx) => {
                        debug!("{token} completed");
                        match tx.send(result) {
                            Ok(_) => (),
                            Err(_) => error!("completion handler gone"),
                        }
                    }
                    None => error!("got unexpected completion {token}"),
                },
                Message::Access(visitor) => {
                    trace!("state access");
                    match visitor.visit(&mut state) {
                        Ok(_) => (),
                        Err(e) => error!("failed to process state access: {e}"),
                    }
                }
                Message::Stop => break,
            }
            trace!("Finished processing message");
        }
        trace!("message loop exited");
    });
    ctx
}
