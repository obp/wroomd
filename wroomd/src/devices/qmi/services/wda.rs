use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct};

#[derive(Clone, Debug, QmiEnum)]
#[repr(u32)]
pub enum LinkLayerProtocol {
    Ethernet = 1,
    IP = 2,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u32)]
pub enum AggregationProtocol {
    Disabled = 0,
    TLP = 1,
    QCNCM = 2,
    MBIM = 3,
    RNDIS = 4,
    QMAP = 5,
    QMAPV2 = 6,
    QMAPV3 = 7,
    QMAPV4 = 8,
    QMAPV5 = 9,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x20]
    SetDataFormat {
        #[id = 0x11]
        link_layer_protocol: Option<LinkLayerProtocol>,
        #[id = 0x12]
        ul_data_agg_protocol: Option<AggregationProtocol>,
        #[id = 0x13]
        dl_data_agg_protocol: Option<AggregationProtocol>,
        #[id = 0x15]
        dl_max_datagrams: Option<u32>,
        #[id = 0x16]
        dl_max_size: Option<u32>,
        #[id = 0x17]
        ep_id: Option<super::EpId>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x20]
    SetDataFormat {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x11]
        link_layer_protocol: Option<LinkLayerProtocol>,
        #[id = 0x12]
        ul_data_agg_protocol: Option<AggregationProtocol>,
        #[id = 0x13]
        dl_data_agg_protocol: Option<AggregationProtocol>,
        #[id = 0x15]
        dl_max_datagrams: Option<u32>,
        #[id = 0x16]
        dl_max_size: Option<u32>,
    },
}
