// The QMI struct macros require all fields, even those that are never read
#![allow(dead_code)]

use super::tlv::parse_tlvs;
use super::*;

#[derive(Debug, Eq, PartialEq, QmiEnum)]
#[repr(u32)]
enum EpType {
    HSIC = 1,
    HSUSB = 2,
    PCIE = 3,
    Embedded = 4,
    BamDmux = 5,
}

#[derive(Debug, QmiStruct)]
struct ControlPort {
    port_name: String,
    ep_type: EpType,
    iface_id: u32,
}

#[derive(Debug, QmiStruct)]
struct HardwareDataPort {
    ep_type: EpType,
    iface_id: u32,
    rx_ep: u32,
    tx_ep: u32,
}

#[derive(Debug, QmiStruct)]
struct SoftwareDataPort {
    ep_type: EpType,
    iface_id: u32,
    port_name: String,
}

#[derive(Debug, QmiMessage)]
#[qmi_request]
enum DpmReq {
    #[id = 0x20]
    OpenPort {
        #[id = 0x10]
        control_ports: Option<Vec<ControlPort>>,
        #[id = 0x11]
        hardware_data_ports: Option<Vec<HardwareDataPort>>,
        #[id = 0x12]
        software_data_ports: Option<Vec<SoftwareDataPort>>,
    },
    #[id = 0x21]
    ClosePort {},
}

#[test]
fn check_result_success_if_no_error() {
    let result = QmiResult {
        result: 0,
        error: 0,
    };
    check_result(Some(&result)).unwrap();
}

#[test]
fn check_result_fails_if_error() {
    let result = QmiResult {
        result: 1,
        error: 3,
    };
    assert_eq!(check_result(Some(&result)).unwrap_err(), Error::Internal);
}

#[test]
fn check_result_fails_if_no_result() {
    assert_eq!(check_result(None).unwrap_err(), Error::MissingResult);
}

#[test]
fn decodes_struct_correctly() {
    let bytes: [u8; 4] = [1, 0, 2, 0];
    let response = struct_from_bytes::<QmiResult>(&bytes).unwrap();
    assert_eq!(response.result, 1);
    assert_eq!(response.error, 2);
}

#[test]
fn encodes_struct_correctly() {
    let response = QmiResult {
        result: 1,
        error: 2,
    };
    assert_eq!(response.to_bytes().unwrap(), [1, 0, 2, 0]);
}

#[derive(Debug, Eq, PartialEq, QmiStruct)]
struct ExampleStruct {
    the_vec: Vec<String>,
}

#[test]
fn can_parse_struct_with_variable_size_vec_items() {
    let msg = ExampleStruct {
        the_vec: vec![String::from("Hello, World!"), String::from("test")],
    };
    let bytes = msg.to_bytes().unwrap();
    let reconstructed: ExampleStruct = struct_from_bytes(&bytes).unwrap();
    assert_eq!(reconstructed, msg);
}

#[derive(Debug, Eq, PartialEq, QmiStruct)]
struct ExampleUnprefixedStruct {
    the_vec: Unprefixed<Vec<Unprefixed<String>>>,
}

#[test]
fn uses_available_space_for_unprefixed_sequences() {
    let msg = ExampleUnprefixedStruct {
        the_vec: Unprefixed(vec![
            Unprefixed(String::from("Hello, World!")),
            Unprefixed(String::from("test")),
        ]),
    };
    let bytes = msg.to_bytes().unwrap();
    let reconstructed: ExampleUnprefixedStruct = struct_from_bytes(&bytes).unwrap();
    assert_eq!(
        reconstructed,
        ExampleUnprefixedStruct {
            the_vec: Unprefixed(vec![Unprefixed(String::from("Hello, World!test"))]),
        }
    );
}

#[test]
fn fails_to_decode_if_too_short() {
    let bytes: [u8; 3] = [1, 0, 2];
    let error: Error = struct_from_bytes::<QmiResult>(&bytes).unwrap_err();
    if error != Error::Eof {
        panic!("Expected Error::Eof, got {}", error);
    }
}

#[test]
fn parses_tlvs_correctly() {
    let bytes: [u8; 43] = [
        16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0, 0, 0, 0, 0, 0, 17, 17,
        0, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ];
    let values = parse_tlvs(&bytes).unwrap();
    assert_eq!(*values.get(&0x10).unwrap(), &bytes[3..23]);
    assert_eq!(*values.get(&0x11).unwrap(), &bytes[26..43]);
    assert!(values.get(&0x12).is_none());
}

#[test]
fn parse_invalid_tlv_fails() {
    let bytes: [u8; 26] = [
        // This is okay
        16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0, 0, 0, 0, 0, 0,
        // This is invalid data
        255, 255, 9,
    ];
    let error: Error = parse_tlvs(&bytes).unwrap_err();
    if error != Error::Eof {
        panic!("Expected Error::Eof, got {}", error);
    }
    let bytes: [u8; 24] = [
        // This is okay
        16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0, 0, 0, 0, 0, 0,
        // This is too short for a header
        255,
    ];
    let error: Error = parse_tlvs(&bytes).unwrap_err();
    if error != Error::Eof {
        panic!("Expected Error::Eof, got {}", error);
    }
}

#[test]
fn decodes_message_correctly() {
    let bytes: [u8; 50] = [
        0, 2, 0, 32, 0, 43, 0, 16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0,
        0, 0, 0, 0, 0, 17, 17, 0, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ];
    let (txn_id, msg) = DpmReq::from_bytes(&bytes).unwrap();
    assert_eq!(txn_id, 2);
    match msg {
        DpmReq::OpenPort {
            control_ports,
            hardware_data_ports,
            software_data_ports,
        } => {
            let cp = &control_ports.unwrap()[0];
            assert_eq!(cp.port_name, "DATA5_CNTL");
            assert_eq!(cp.ep_type, EpType::BamDmux);
            assert!(hardware_data_ports.is_some());
            assert!(software_data_ports.is_none());
        }
        _ => panic!("Did not decode as open_port request"),
    }
}

#[test]
fn checks_message_type() {
    let bytes: [u8; 50] = [
        2, 2, 0, 32, 0, 43, 0, 16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0,
        0, 0, 0, 0, 0, 17, 17, 0, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ];
    let error: Error = DpmReq::from_bytes(&bytes).unwrap_err();
    if error != Error::UnexpectedType {
        panic!("Expected Error::UnexpectedType, got {}", error);
    }
}

#[test]
fn encodes_message_correctly() {
    let bytes: [u8; 30] = [
        0, 2, 0, 32, 0, 23, 0, 16, 20, 0, 1, 10, 68, 65, 84, 65, 53, 95, 67, 78, 84, 76, 5, 0, 0,
        0, 0, 0, 0, 0,
    ];
    let msg = DpmReq::OpenPort {
        control_ports: Some(vec![ControlPort {
            port_name: String::from("DATA5_CNTL"),
            ep_type: EpType::BamDmux,
            iface_id: 0,
        }]),
        hardware_data_ports: None,
        software_data_ports: None,
    };
    assert_eq!(msg.to_bytes(2).unwrap(), bytes);
}
