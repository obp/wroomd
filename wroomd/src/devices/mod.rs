use futures::future::BoxFuture;
use ipnet::{Ipv4Net, Ipv6Net};
use std::collections::HashMap;
use std::error::Error;
use std::fmt::{self, Display};
use std::net::{Ipv4Addr, Ipv6Addr};

pub mod qmi;
pub mod udev;
pub mod util;

use self::qmi::services::QmiService;

#[derive(Debug, Hash, Clone, Copy, Eq, PartialEq)]
pub enum DeviceID {
    QRTR,
}

impl Display for DeviceID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DeviceID::QRTR => f.write_str("qrtr"),
        }
    }
}

#[derive(Debug, Hash, Clone, Copy, Eq, PartialEq)]
pub enum PortIndex {
    Single,
    Mux(u8),
}

#[derive(Debug, Hash, Clone, Copy, Eq, PartialEq)]
pub struct PortID {
    pub device: DeviceID,
    pub port: PortIndex,
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
pub struct SlotID {
    pub device: DeviceID,
    pub slot: u8,
}

impl Display for SlotID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/{}", self.device, self.slot)
    }
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
pub struct CallID {
    pub slot: SlotID,
    pub call: u8,
}

impl Display for CallID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/call/{}", self.slot, self.call)
    }
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
pub struct DataNetworkID {
    pub slot: SlotID,
    pub network: u8,
}

impl Display for DataNetworkID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}/net/{}", self.slot, self.network)
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Lock {
    None,
    PIN,
    PUK,
    Personalization,
    Permanent,
}

impl Display for Lock {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Lock::None => "none",
            Lock::PIN => "pin",
            Lock::PUK => "puk",
            Lock::Personalization => "personalization",
            Lock::Permanent => "permanent",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum CodeKind {
    PIN1,
    PIN2,
    UPIN,
}

impl Display for CodeKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            CodeKind::PIN1 => "pin1",
            CodeKind::PIN2 => "pin2",
            CodeKind::UPIN => "upin",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Code {
    pub kind: CodeKind,
    pub retries: u8,
    pub unblock_retries: u8,
    pub verified: bool,
}

#[derive(Clone, Debug)]
pub struct CardStatus {
    pub priority: u32,
    pub lock: Lock,
    pub codes: Vec<Code>,
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum NetworkState {
    #[default]
    Unavailable,
    Searching,
    Denied,
    Registered,
}

impl Display for NetworkState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            NetworkState::Unavailable => "unavailable",
            NetworkState::Searching => "searching",
            NetworkState::Denied => "denied",
            NetworkState::Registered => "registered",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum NetworkTechnology {
    #[default]
    None,
    GPRS,
    EDGE,
    UMTS,
    HSDPA,
    HSUPA,
    GSM,
    LTE,
    HSPAPlus,
}

impl Display for NetworkTechnology {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            NetworkTechnology::None => "none",
            NetworkTechnology::GPRS => "gprs",
            NetworkTechnology::EDGE => "edge",
            NetworkTechnology::UMTS => "umts",
            NetworkTechnology::HSDPA => "hsdpa",
            NetworkTechnology::HSUPA => "hsupa",
            NetworkTechnology::GSM => "gsm",
            NetworkTechnology::LTE => "lte",
            NetworkTechnology::HSPAPlus => "hspa+",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Default)]
pub struct NetworkStatus {
    pub signal: u8,
    pub state: NetworkState,
    pub technology: NetworkTechnology,
    pub operator: String,
    pub roaming: bool,
}

#[derive(Clone, Debug, Default)]
pub struct SlotStatus {
    pub card: Option<CardStatus>,
    pub network: NetworkStatus,
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum Direction {
    #[default]
    Unknown,
    Outgoing,
    Incoming,
}

impl Display for Direction {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Direction::Unknown => "unknown",
            Direction::Outgoing => "outgoing",
            Direction::Incoming => "incoming",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum CallState {
    #[default]
    Unknown,
    Initializing,
    Ringing,
    Active,
    Hold,
    Waiting,
    Ending,
}

impl Display for CallState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            CallState::Unknown => "unknown",
            CallState::Initializing => "initializing",
            CallState::Ringing => "ringing",
            CallState::Active => "active",
            CallState::Hold => "hold",
            CallState::Waiting => "waiting",
            CallState::Ending => "ending",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Default)]
pub struct CallStatus {
    pub number: String,
    pub direction: Direction,
    pub state: CallState,
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub enum DataNetworkState {
    #[default]
    Idle,
    Configuring,
    Active,
    Paused,
}

impl Display for DataNetworkState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            DataNetworkState::Idle => "idle",
            DataNetworkState::Configuring => "configuring",
            DataNetworkState::Active => "active",
            DataNetworkState::Paused => "paused",
        };
        f.write_str(s)
    }
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct DataConfigIpv4 {
    pub ifname: Option<String>,
    pub port: Option<PortID>,
    pub addr: Option<Ipv4Net>,
    pub gateway: Option<Ipv4Addr>,
    pub dns: Option<Ipv4Addr>,
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct DataConfigIpv6 {
    pub ifname: Option<String>,
    pub port: Option<PortID>,
    pub addr: Option<Ipv6Net>,
    pub gateway: Option<Ipv6Addr>,
    pub dns: Option<Ipv6Addr>,
}

#[derive(Clone, Debug, Default)]
pub struct DataNetworkStatus {
    pub apn: String,
    pub state_ipv4: DataNetworkState,
    pub state_ipv6: DataNetworkState,
    pub config_ipv4: DataConfigIpv4,
    pub config_ipv6: DataConfigIpv6,
}

impl DataNetworkStatus {
    pub fn resolve_ports(self, ports: &HashMap<PortID, String>) -> Self {
        DataNetworkStatus {
            config_ipv4: DataConfigIpv4 {
                ifname: match &self.config_ipv4.port {
                    Some(port) => ports.get(port).cloned(),
                    None => None,
                },
                ..self.config_ipv4
            },
            config_ipv6: DataConfigIpv6 {
                ifname: match &self.config_ipv6.port {
                    Some(port) => ports.get(port).cloned(),
                    None => None,
                },
                ..self.config_ipv6
            },
            ..self
        }
    }
}

#[derive(Debug)]
pub enum Action {
    QrtrInit,
    QmiInitService(QmiService),
    QmiReloadCardStatus,
    QmiReloadNetwork(u8, u8),
    QmiReloadIpConfig(self::qmi::Client),
    ActivateCard(SlotID),
    DeactivateCard(SlotID),
    UnlockCard {
        slot: SlotID,
        code_kind: CodeKind,
        code: String,
    },
    UnblockCard {
        slot: SlotID,
        code_kind: CodeKind,
        puk: String,
        new_code: String,
    },
    Dial {
        slot: SlotID,
        number: String,
    },
    Accept(CallID),
    Hangup(CallID),
    StartNetwork(DataNetworkID),
    StopNetwork(DataNetworkID),
}

#[derive(Debug)]
pub enum ActionError {
    Other(String),
    Qmi(::qmi::Error),
    QmiServiceNotFound,
    NotMapped,
    DeviceUnsupported,
}

impl From<std::io::Error> for ActionError {
    fn from(e: std::io::Error) -> Self {
        ActionError::Other(e.to_string())
    }
}

impl From<zbus::Error> for ActionError {
    fn from(e: zbus::Error) -> Self {
        ActionError::Other(e.to_string())
    }
}

impl From<::qmi::Error> for ActionError {
    fn from(e: ::qmi::Error) -> Self {
        ActionError::Qmi(e)
    }
}

impl Display for ActionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ActionError::Other(msg) => f.write_str(msg),
            ActionError::Qmi(err) => write!(f, "QMI error: {err}"),
            ActionError::QmiServiceNotFound => f.write_str("QMI service not found"),
            ActionError::NotMapped => {
                f.write_str("entity (network, slot or subscription, etc.) not mapped")
            }
            ActionError::DeviceUnsupported => f.write_str("operation unsupported by device type"),
        }
    }
}

impl Error for ActionError {}

pub trait DeviceHandle {
    fn action(self, act: Action) -> BoxFuture<'static, Result<(), ActionError>>;
}
