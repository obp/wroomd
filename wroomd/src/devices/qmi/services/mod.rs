use qmi::{QmiEnum, QmiStruct};

pub mod dms;
pub mod dpm;
pub mod nas;
pub mod uim;
pub mod voice;
pub mod wda;
pub mod wds;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, QmiEnum)]
#[repr(u32)]
pub enum QmiService {
    CTL = 0,
    WDS = 1,
    DMS = 2,
    NAS = 3,
    QOS = 4,
    WMS = 5,
    Auth = 7,
    AT = 8,
    Voice = 9,
    CAT = 10,
    UIM = 11,
    PBM = 12,
    RMTFS = 14,
    Test = 15,
    LOC = 16,
    SAR = 17,
    IMSS = 18,
    Time = 22,
    TS = 23,
    TMD = 24,
    WDA = 26,
    CSVT = 29,
    IMSA = 33,
    COEX = 34,
    PDC = 36,
    RFRPE = 41,
    DSD = 42,
    SSCTL = 43,
    DPM = 47,
    DFS = 48,
    OTT = 68,
    UIMHttp = 71,
    IMSPrivate = 77,
    SLIMbus = 769,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum Subscription {
    Primary = 0,
    Secondary = 1,
    Tertiary = 2,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u32)]
pub enum EpType {
    HSIC = 1,
    HSUSB = 2,
    PCIE = 3,
    Embedded = 4,
    BamDmux = 5,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct EpId {
    pub ty: EpType,
    pub iface: u32,
}
