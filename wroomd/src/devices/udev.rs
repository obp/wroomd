use crate::context::Event;
use futures::stream::TryStreamExt;
use log::{debug, error};
use std::str::FromStr;
use super::{DeviceID, PortID, PortIndex};
use tokio_udev::{AsyncMonitorSocket, Device, Enumerator, EventType, MonitorBuilder};

type EventSender = futures::channel::mpsc::UnboundedSender<Event>;

fn send_event(sender: &EventSender, dev: &Device, name: &str, id: DeviceID) {
    let mux_port = dev.property_value("WROOMD_MUX_PORT")
        .and_then(|p| p.to_str())
        .and_then(|p| u8::from_str(p).ok());
    let port_id = PortID {
        device: id,
        port: match mux_port {
            None => PortIndex::Single,
            Some(i) => PortIndex::Mux(i),
        },
    };
    match sender.unbounded_send(Event::NetPort(port_id, name.into())) {
        Ok(_) => debug!("sent port update"),
        Err(e) => error!("failed to send port update: {e}"),
    }
}

fn process_device(sender: &EventSender, dev: Device) {
    debug!("found device: {dev:#?}");
    match dev.sysname().to_str() {
        Some(name) => {
            if dev.property_value("WROOMD_USE_QRTR") == Some("1".as_ref()) {
                send_event(sender, &dev, name, DeviceID::QRTR);
            }
        }
        None => error!("failed to decode device name"),
    }
}

pub fn enumerate(sender: &EventSender) -> std::io::Result<()> {
    let mut e = Enumerator::new()?;
    e.match_subsystem("net")?;
    e.scan_devices()?.for_each(move |d| process_device(sender, d));
    Ok(())
}

pub async fn try_monitor_blocking(sender: &EventSender) -> std::io::Result<()> {
    enumerate(sender)?;
    let mut socket: AsyncMonitorSocket = MonitorBuilder::new()?
        .match_subsystem("net")?
        .listen()?
        .try_into()?;
    while let Some(ev) = socket.try_next().await? {
        match ev.event_type() {
            EventType::Remove => debug!("device removed: {:#?}", ev.device()),
            _ => process_device(&sender, ev.device()),
        }
    }
    Ok(())
}
