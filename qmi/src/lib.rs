mod error;
mod r#struct;
pub mod tlv;

pub use qmi_macros::*;

pub use error::{Error, Result};
pub use r#struct::{QmiStruct, Unprefixed};

use crate as qmi; // needed for the macros

#[derive(Clone, Debug, QmiStruct)]
pub struct QmiResult {
    pub result: u16,
    pub error: u16,
}

#[derive(Debug, QmiStruct)]
pub struct QmiHeader {
    pub ty: u8,
    pub txn_id: u16,
    pub msg_id: u16,
    pub msg_len: u16,
}

pub trait QmiMessage: Sized {
    fn from_bytes(bytes: &[u8]) -> Result<(u16, Self)>;
    fn to_bytes(&self, txn_id: u16) -> Result<Vec<u8>>;
}

pub fn check_result(res_opt: Option<&QmiResult>) -> Result<()> {
    let res = res_opt.ok_or(Error::MissingResult)?;
    match res.error {
        0 => Ok(()),
        1 => Err(Error::MalformedMessage),
        2 => Err(Error::NoMemory),
        3 => Err(Error::Internal),
        4 => Err(Error::Aborted),
        5 => Err(Error::ClientIdsExhausted),
        6 => Err(Error::UnabortableTransaction),
        7 => Err(Error::InvalidClientId),
        8 => Err(Error::NoThresholdsProvided),
        9 => Err(Error::InvalidHandle),
        10 => Err(Error::InvalidProfile),
        11 => Err(Error::InvalidPinId),
        12 => Err(Error::IncorrectPin),
        13 => Err(Error::NoNetworkFound),
        14 => Err(Error::CallFailed),
        15 => Err(Error::OutOfCall),
        16 => Err(Error::NotProvisioned),
        17 => Err(Error::MissingArgument),
        19 => Err(Error::ArgumentTooLong),
        22 => Err(Error::InvalidTransactionId),
        23 => Err(Error::DeviceInUse),
        24 => Err(Error::NetworkUnsupported),
        25 => Err(Error::DeviceUnsupported),
        26 => Err(Error::NoEffect),
        27 => Err(Error::NoFreeProfile),
        28 => Err(Error::InvalidPdpType),
        29 => Err(Error::InvalidTechnologyPreference),
        30 => Err(Error::InvalidProfileType),
        31 => Err(Error::InvalidServiceType),
        32 => Err(Error::InvalidRegisterAction),
        33 => Err(Error::InvalidPsAttachAction),
        34 => Err(Error::AuthenticationFailed),
        35 => Err(Error::PinBlocked),
        36 => Err(Error::PinAlwaysBlocked),
        37 => Err(Error::UimUninitialized),
        38 => Err(Error::MaximumQosRequestsInUse),
        39 => Err(Error::IncorrectFlowFilter),
        40 => Err(Error::NetworkQosUnaware),
        41 => Err(Error::InvalidQosId),
        42 => Err(Error::RequestedNumberUnsupported),
        43 => Err(Error::InterfaceNotFound),
        44 => Err(Error::FlowSuspended),
        45 => Err(Error::InvalidDataFormat),
        46 => Err(Error::GeneralError),
        47 => Err(Error::UnknownError),
        48 => Err(Error::InvalidArgument),
        49 => Err(Error::InvalidIndex),
        50 => Err(Error::NoEntry),
        51 => Err(Error::DeviceStorageFull),
        52 => Err(Error::DeviceNotReady),
        53 => Err(Error::NetworkNotReady),
        54 => Err(Error::WmsCauseCode),
        55 => Err(Error::WmsMessageNotSent),
        56 => Err(Error::WmsMessageDeliveryFailure),
        57 => Err(Error::WmsInvalidMessageId),
        58 => Err(Error::WmsEncoding),
        59 => Err(Error::AuthenticationLock),
        60 => Err(Error::InvalidTransition),
        61 => Err(Error::NotMcastInterface),
        62 => Err(Error::MaximumMcastRequestsInUse),
        63 => Err(Error::InvalidMcastHandle),
        64 => Err(Error::InvalidIpFamilyPreference),
        65 => Err(Error::SessionInactive),
        66 => Err(Error::SessionInvalid),
        67 => Err(Error::SessionOwnership),
        68 => Err(Error::InsufficientResources),
        69 => Err(Error::Disabled),
        70 => Err(Error::InvalidOperation),
        71 => Err(Error::InvalidQmiCommand),
        72 => Err(Error::WmsTPduType),
        73 => Err(Error::WmsSmscAddress),
        74 => Err(Error::InformationUnavailable),
        75 => Err(Error::SegmentTooLong),
        76 => Err(Error::SegmentOrder),
        77 => Err(Error::BundlingNotSupported),
        78 => Err(Error::OperationPartialFailure),
        79 => Err(Error::PolicyMismatch),
        80 => Err(Error::SimFileNotFound),
        81 => Err(Error::ExtendedInternal),
        82 => Err(Error::AccessDenied),
        83 => Err(Error::HardwareRestricted),
        84 => Err(Error::AckNotSent),
        85 => Err(Error::InjectTimeout),
        90 => Err(Error::IncompatibleState),
        91 => Err(Error::FdnRestrict),
        92 => Err(Error::SupsFailureCase),
        93 => Err(Error::NoRadio),
        94 => Err(Error::NotSupported),
        95 => Err(Error::NoSubscription),
        96 => Err(Error::CardCallControlFailed),
        97 => Err(Error::NetworkAborted),
        98 => Err(Error::MsgBlocked),
        100 => Err(Error::InvalidSessionType),
        101 => Err(Error::InvalidPbType),
        102 => Err(Error::NoSim),
        103 => Err(Error::PbNotReady),
        104 => Err(Error::PinRestriction),
        105 => Err(Error::Pin1Restriction),
        106 => Err(Error::PukRestriction),
        107 => Err(Error::Puk2Restriction),
        108 => Err(Error::PbAccessRestricted),
        109 => Err(Error::PbDeleteInProgress),
        110 => Err(Error::PbTextTooLong),
        111 => Err(Error::PbNumberTooLong),
        112 => Err(Error::PbHiddenKeyRestriction),
        113 => Err(Error::PbNotAvailable),
        114 => Err(Error::DeviceMemoryError),
        115 => Err(Error::NoPermission),
        116 => Err(Error::TooSoon),
        117 => Err(Error::TimeNotAcquired),
        118 => Err(Error::OperationInProgress),
        388 => Err(Error::FwWriteFailed),
        389 => Err(Error::FwInfoReadFailed),
        390 => Err(Error::FwFileNotFound),
        391 => Err(Error::FwDirNotFound),
        392 => Err(Error::FwAlreadyActivated),
        393 => Err(Error::FwCannotGenericImage),
        400 => Err(Error::FwFileOpenFailed),
        401 => Err(Error::FwUpdateDiscontinuousFrame),
        402 => Err(Error::FwUpdateFailed),
        61441 => Err(Error::CatEventRegistrationFailed),
        61442 => Err(Error::CatInvalidTerminalResponse),
        61443 => Err(Error::CatInvalidEnvelopeCommand),
        61444 => Err(Error::CatEnvelopeCommandBusy),
        61445 => Err(Error::CatEnvelopeCommandFailed),
        _ => Err(Error::InvalidQmiError(res.error)),
    }
}

pub fn first_struct_from_bytes<T>(bytes: &[u8]) -> Result<(T, &[u8])>
where
    T: QmiStruct,
{
    T::from_bytes(bytes)
}

pub fn struct_from_bytes<T>(bytes: &[u8]) -> Result<T>
where
    T: QmiStruct,
{
    let (s, rest) = T::from_bytes(bytes)?;
    if rest.is_empty() {
        Ok(s)
    } else {
        Err(Error::TrailingBytes)
    }
}

#[cfg(test)]
mod tests;
