use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct, Unprefixed};

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum ProfileType {
    _3GPP = 0,
    _3GPP2 = 1,
    EPC = 2,
}

#[derive(Clone, Copy, Debug, QmiEnum)]
#[repr(u32)]
pub enum Subscription {
    Primary = 1,
    Secondary = 2,
    Tertiary = 3,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum IpFamily {
    Ipv4 = 4,
    Ipv6 = 6,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum ConnectionStatus {
    Unknown = 0,
    Disconnected = 1,
    Connected = 2,
    Suspended = 3,
    Authenticating = 4,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct ProfileId {
    pub ty: ProfileType,
    pub index: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct Profile {
    pub id: ProfileId,
    pub name: String,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct Ipv4Addr {
    pub _3: u8,
    pub _2: u8,
    pub _1: u8,
    pub _0: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct Ipv6Addr {
    pub _0: u8,
    pub _1: u8,
    pub _2: u8,
    pub _3: u8,
    pub _4: u8,
    pub _5: u8,
    pub _6: u8,
    pub _7: u8,
    pub _8: u8,
    pub _9: u8,
    pub _a: u8,
    pub _b: u8,
    pub _c: u8,
    pub _d: u8,
    pub _e: u8,
    pub _f: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct Ipv6Net {
    pub addr: Ipv6Addr,
    pub prefix_len: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct ConnectionStatusUpdate {
    pub status: ConnectionStatus,
    pub reconfig_required: u8,
}

pub const REQUEST_IP_ADDRESS: u32 = 1 << 8;
pub const REQUEST_GATEWAY_INFO: u32 = 1 << 9;

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x12]
        ext_ip_config_change: Option<u8>,
        #[id = 0x19]
        profile_change: Option<u8>,
    },
    #[id = 0x20]
    StartNetwork {
        #[id = 0x31]
        profile_index_3gpp: Option<u8>,
    },
    #[id = 0x2A]
    GetProfileList {
        #[id = 0x10]
        profile_type: Option<ProfileType>,
    },
    #[id = 0x2B]
    GetProfileSettings {
        #[id = 0x01]
        id: Option<ProfileId>,
    },
    #[id = 0x2D]
    GetCurrentSettings {
        #[id = 0x10]
        requested_settings: Option<u32>,
    },
    #[id = 0x4D]
    SetIpFamily {
        #[id = 0x01]
        ip_family: Option<IpFamily>,
    },
    #[id = 0xA2]
    BindMuxDataPort {
        #[id = 0x10]
        ep_id: Option<super::EpId>,
        #[id = 0x11]
        mux_id: Option<u8>,
    },
    #[id = 0xAF]
    BindSubscription {
        #[id = 0x01]
        subscription: Option<Subscription>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x20]
    StartNetwork {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x01]
        packet_data_handle: Option<u32>,
    },
    #[id = 0x2A]
    GetProfileList {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x01]
        profile_list: Option<Vec<Profile>>,
        #[id = 0xE0]
        err_code: Option<u16>,
    },
    #[id = 0x2B]
    GetProfileSettings {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x14]
        apn_name: Option<Unprefixed<String>>,
        #[id = 0xE0]
        err_code: Option<u16>,
    },
    #[id = 0x2D]
    GetCurrentSettings {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x1E]
        ip_addr: Option<Ipv4Addr>,
        #[id = 0x20]
        ip_gateway: Option<Ipv4Addr>,
        #[id = 0x21]
        ip_netmask: Option<Ipv4Addr>,
        #[id = 0x25]
        ipv6_addr: Option<Ipv6Net>,
        #[id = 0x26]
        ipv6_gateway: Option<Ipv6Net>,
    },
    #[id = 0x4D]
    SetIpFamily {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0xA2]
    BindMuxDataPort {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0xAF]
    BindSubscription {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_indication]
pub enum Ind {
    #[id = 0x22]
    PacketServiceStatus {
        #[id = 0x01]
        status: Option<ConnectionStatusUpdate>,
        #[id = 0x10]
        end_reason: Option<u16>,
        #[id = 0x12]
        ip_family: Option<IpFamily>,
    },
    #[id = 0x8C]
    ExtIpConfig {
        #[id = 0x10]
        changed_settings: Option<u32>,
    },
}
