# wroomd

Wroomd is a mobile modem manager daemon written in Rust.

Unlike ModemManager, it supports multi-SIM devices by design, as its D-Bus API is centered around SIM slots and not modem devices.

Currently, wroomd only supports Qualcomm SoCs with QRTR and has the following features:
 - SIM management and unlocking
 - Voice call management (not linked to audio stack yet, would require another daemon)
 - Mobile network management

## Usage

To set up wroomd, copy the files in udev to `/lib/udev/rules.d` and create a file at `/etc/dbus-1/systemd.d/de.abscue.obp.Wroomd.conf` with the following content:

```xml
<!DOCTYPE busconfig PUBLIC
 "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
 "http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">
<busconfig>
  <policy user="root">
    <allow own="de.abscue.obp.Wroomd"/>
    <allow send_destination="de.abscue.obp.Wroomd"/>
  </policy>
</busconfig>
```

Authorization via polkit is not implemented yet.

After reloading the D-Bus configuration and starting wroomd, you can get more information about the API by introspecting `de.abscue.obp.Wroomd`. For example:

```console
# gdbus introspect -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd
```

### SIM unlocking
```console
# gdbus call -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0 -m de.abscue.obp.Wroomd.Slot.Activate # important on DSDS devices - makes the slot primary
# gdbus call -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0 -m de.abscue.obp.Wroomd.Slot.Unlock "pin1" "1234"
```

### Mobile data network
```console
# gdbus call -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0/net/1 -m de.abscue.obp.Wroomd.Network.Start
# gdbus introspect -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0/net/1 -p # shows the IP configuration
# ip l set <interface_name> up arp on
# ip a add <ip_address> dev <interface_name> # Note: DHCP will also work with most providers.
# ip r add default via <gateway> dev <interface_name>
# # IPv6 should work out of the box, but if it doesn't:
# ip -6 a add <ipv6_address> dev <interface_name>
```

### Voice calls
```console
# gdbus call -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0 -m de.abscue.obp.Wroomd.Slot.Dial "+123456789"
# gdbus introspect -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0/call -p -r # shows the current call status
# gdbus call -y -d de.abscue.obp.Wroomd -o /de/abscue/obp/Wroomd/qrtr/0/call/1 -m de.abscue.obp.Wroomd.Call.Hangup
```

## Repository structure

The wroomd project consists of multiple crates, all located in this repository:
 - `qmi/`: Pure functions needed for constructing and parsing QMI messages, and nothing else
 - `qmi-macros/`: Derive macros for defining QMI structures and messages
 - `qrtr/`: Rust interface for QRTR sockets, based on tokio. Contains nothing related to the QMI protocol
 - `wroomd/`: The daemon itself

The daemon consists of a D-Bus interface implementation (`wroomd/src/dbus.rs`), a device abstraction layer (`wroomd/src/devices/`) and multiple event loops managing events, actions and the state of devices (`wroomd/src/state.rs`, `wroomd/src/context.rs`).

Each device type has:
 - its own part of the state
 - one or more modules in `wroomd/src/devices/`, which:
   - establish a connection to the device
   - process incoming messages from the device
   - convert wroomd's actions into outgoing messages

Only one device type is currently implemented, so the infrastructure for supporting multiple devices is still missing a few things, although it has improved since the first commit.
