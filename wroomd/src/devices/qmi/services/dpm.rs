use qmi::{QmiMessage, QmiResult, QmiStruct};

#[derive(Clone, Debug, QmiStruct)]
pub struct ControlPort {
    pub port_name: String,
    pub ep_id: super::EpId,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct HardwareDataPort {
    pub ep_id: super::EpId,
    pub rx_ep: u32,
    pub tx_ep: u32,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct SoftwareDataPort {
    pub ep_id: super::EpId,
    pub port_name: String,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x20]
    OpenPort {
        #[id = 0x10]
        control_ports: Option<Vec<ControlPort>>,
        #[id = 0x11]
        hardware_data_ports: Option<Vec<HardwareDataPort>>,
        #[id = 0x12]
        software_data_ports: Option<Vec<SoftwareDataPort>>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x20]
    OpenPort {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}
