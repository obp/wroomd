use super::services::QmiService;
use super::{Client, QmiDevice};
use crate::context::{spawn_context, Event, ResultSender, StateContext};
use crate::devices::{Action, ActionError, DeviceHandle, DeviceID};
use futures::channel::mpsc;
use futures::future::BoxFuture;
use futures::stream::{AbortHandle, Abortable, StreamExt, TryStreamExt};
use log::{debug, error, trace, warn};
use qmi::{self, QmiEnum, QmiStruct};
use qrtr::{self, QrtrMessage, QrtrSocket};
use std::collections::HashMap;
use std::io;

const QRTR_PORT_CTRL: u32 = 0xfffffffe;

#[derive(Debug, QmiEnum)]
#[repr(u32)]
enum CtrlPacketType {
    Data = 1,
    Hello = 2,
    Bye = 3,
    NewServer = 4,
    DelServer = 5,
    DelClient = 6,
    ResumeTx = 7,
    Exit = 8,
    Ping = 9,
    NewLookup = 10,
    DelLookup = 11,
}

#[derive(Debug, QmiStruct)]
struct QrtrServer {
    service: QmiService,
    instance: u32,
    node: u32,
    port: u32,
}

async fn process_ctrl_packet(ctx: &QrtrBundle, msg: &QrtrMessage) -> Result<(), ActionError> {
    let pktype: CtrlPacketType = qmi::struct_from_bytes(&msg.data[..4])?;
    if matches!(pktype, CtrlPacketType::NewServer) {
        let server: QrtrServer = qmi::struct_from_bytes(&msg.data[4..])?;
        // Node 0 is assumed to be the modem
        if (server.instance >> 8) == 0 && server.node == 0 {
            let action = Action::QmiInitService(server.service);
            ctx.0
                .with(move |state| {
                    state.ports.insert(server.service, server.port);
                    state.services.insert(server.port, server.service);
                })
                .await?;
            ctx.0.action(action).await
        } else {
            Ok(())
        }
    } else {
        Ok(())
    }
}

async fn process_message(
    ctx: QrtrBundle,
    dev: DeviceID,
    cid: Client,
    msg: &QrtrMessage,
) -> Result<(), ActionError> {
    if msg.port == QRTR_PORT_CTRL {
        process_ctrl_packet(&ctx, msg).await
    } else if msg.node == 0 {
        trace!("({:?}) Message: {:?}", cid, msg);
        let port = msg.port;
        let service = ctx
            .0
            .with(move |state| state.services.get(&port).copied())
            .await?;
        trace!("Message service: {:?}", service);
        match service {
            Some(service) => {
                super::common::process_msg(Box::new(ctx), dev, cid, service, &msg.data).await
            }
            None => Ok(()),
        }
    } else {
        Ok(())
    }
}

enum QrtrCmd {
    Msg(QrtrMessage),
    Close,
}

enum QrtrErr {
    Fail(io::Error),
    Close,
}

fn spawn_socket(
    ctx: QrtrBundle,
    dev: DeviceID,
    cid: Client,
) -> io::Result<mpsc::UnboundedSender<QrtrCmd>> {
    debug!("Opening QRTR {:?} socket", cid);
    let socket = std::sync::Arc::new(QrtrSocket::new()?);
    let socket2 = socket.clone();
    let (abort_handle, abort_reg) = AbortHandle::new_pair();
    let (sender, receiver) = mpsc::unbounded();
    tokio::spawn(async move {
        let result = receiver
            .map(Ok)
            .try_for_each(|cmd| async {
                match cmd {
                    QrtrCmd::Msg(msg) => match socket.write(msg).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(QrtrErr::Fail(e)),
                    },
                    QrtrCmd::Close => Err(QrtrErr::Close),
                }
            })
            .await;
        match result {
            Ok(_) => error!("unexpected end of stream"),
            Err(QrtrErr::Fail(e)) => error!("failed to send QRTR message: {e}"),
            Err(QrtrErr::Close) => abort_handle.abort(),
        }
    });
    tokio::spawn(async move {
        let stream = Abortable::new(socket2.stream(), abort_reg);
        stream
            .for_each(|result| async {
                match result {
                    Ok(msg) => {
                        let ctx = ctx.clone();
                        tokio::spawn(async move {
                            match process_message(ctx, dev, cid, &msg).await {
                                Ok(_) => (),
                                Err(ActionError::NotMapped) => (), // avoid log spam
                                Err(e) => warn!("failed to process QRTR message: {e}"),
                            }
                        });
                    }
                    Err(e) => error!("failed to receive QRTR message: {e}"),
                }
            })
            .await;
        debug!("Closed QRTR {:?} socket", cid);
    });
    Ok(sender)
}

pub struct QrtrDevice {
    id: DeviceID,
    senders: HashMap<Client, mpsc::UnboundedSender<QrtrCmd>>,
    ports: HashMap<QmiService, u32>,
    services: HashMap<u32, QmiService>,
}

impl QrtrDevice {
    fn client(
        &mut self,
        ctx: &QrtrBundle,
        cid: Client,
    ) -> io::Result<mpsc::UnboundedSender<QrtrCmd>> {
        match self.senders.get(&cid) {
            Some(sender) => Ok(sender.clone()),
            None => {
                let sender = spawn_socket(ctx.clone(), self.id, cid)?;
                self.senders.insert(cid, sender.clone());
                Ok(sender)
            }
        }
    }

    fn init(&mut self, ctx: &QrtrBundle) -> Result<(), ActionError> {
        let msg = CtrlPacketType::NewLookup;
        self.client(ctx, Client::Subs(0))?
            .unbounded_send(QrtrCmd::Msg(QrtrMessage {
                node: 1,
                port: QRTR_PORT_CTRL,
                data: msg.to_bytes()?,
            }))
            .map_err(|e| ActionError::Other(format!("failed to send QRTR message: {e}")))
    }
}

#[derive(Clone)]
pub struct QrtrBundle(StateContext<QrtrDevice>, StateContext<super::State>);

impl QrtrBundle {
    pub fn new(
        id: DeviceID,
        action_sender: mpsc::UnboundedSender<(Action, Option<ResultSender>)>,
        dbus_sender: mpsc::UnboundedSender<Event>,
    ) -> Self {
        QrtrBundle(
            spawn_context(
                QrtrDevice {
                    id,
                    senders: HashMap::new(),
                    ports: HashMap::new(),
                    services: HashMap::new(),
                },
                action_sender.clone(),
                dbus_sender.clone(),
            ),
            spawn_context(super::State::default(), action_sender, dbus_sender),
        )
    }
}

impl QmiDevice for QrtrBundle {
    fn close(&self, cid: Client) -> BoxFuture<'_, ()> {
        Box::pin(async move {
            match self.0.with(move |q| q.senders.remove(&cid)).await {
                Ok(None) => warn!("closing nonexistent client"),
                Ok(Some(tx)) => match tx.unbounded_send(QrtrCmd::Close) {
                    Ok(_) => (),
                    Err(e) => warn!("sending close message failed: {e}"),
                },
                Err(e) => warn!("getting sender to close client failed: {e}"),
            }
        })
    }

    fn send_msg(
        &self,
        cid: Client,
        service: QmiService,
        data: Vec<u8>,
    ) -> BoxFuture<'_, Result<(), ActionError>> {
        Box::pin(async move {
            let handle = self.clone();
            self.0
                .with(move |q| {
                    let msg = match q.ports.get(&service) {
                        Some(port) => Ok(QrtrMessage {
                            node: 0,
                            port: *port,
                            data,
                        }),
                        None => Err(ActionError::QmiServiceNotFound),
                    }?;
                    q.client(&handle, cid)?
                        .unbounded_send(QrtrCmd::Msg(msg))
                        .map_err(|e| {
                            ActionError::Other(format!("failed to send QRTR message: {e}"))
                        })
                })
                .await?
        })
    }

    fn state(&self) -> &'_ StateContext<super::State> {
        &self.1
    }
}

impl DeviceHandle for QrtrBundle {
    fn action(self, act: Action) -> BoxFuture<'static, Result<(), ActionError>> {
        Box::pin(async move {
            match act {
                Action::QrtrInit => self.0.clone().with(move |q| q.init(&self)).await?,
                _ => {
                    let id = self.0.with(|q| q.id).await?;
                    super::common::action(Box::new(self.clone()), id, act).await
                }
            }
        })
    }
}
