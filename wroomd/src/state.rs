use crate::context::{spawn_context, Event, ResultSender};
use crate::dbus;
use crate::devices::{self, Action, DeviceHandle, DeviceID, PortID};
use futures::channel::mpsc;
use futures::stream::{self, StreamExt};
use log::{debug, error, trace};
use std::collections::HashMap;
use zbus::Connection;

pub async fn track_state(conn: Connection) {
    let (action_tx, action_rx) = mpsc::unbounded::<(Action, Option<ResultSender>)>();
    let (event_tx, event_rx) = mpsc::unbounded::<Event>();
    let init_stream = stream::iter(vec![(Action::QrtrInit, None)]);
    let mut stream = Box::pin(init_stream.chain(action_rx));

    // There is only one QRTR device. TODO: implement more device types
    let qrtr =
        devices::qmi::qrtr::QrtrBundle::new(DeviceID::QRTR, action_tx.clone(), event_tx.clone());

    let dbus_tx = event_tx.clone();
    tokio::spawn(async move {
        let mut ports = HashMap::<PortID, String>::new();
        let ctx = spawn_context((), action_tx, dbus_tx);
        let mut stream = Box::pin(event_rx);
        while let Some(event) = stream.next().await {
            match event {
                Event::UpdateSlot(id, update, tx) => {
                    trace!("[D-Bus] Event::UpdateSlot {:#?}", id);
                    let r = match dbus::update_slot(&conn, &ctx, &id, update).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e.into()),
                    };
                    match tx.send(r) {
                        Ok(_) => (),
                        Err(_) => error!("slot update completion handler gone"),
                    }
                }
                Event::UpdateCall(id, update, tx) => {
                    trace!("[D-Bus] Event::UpdateCall {:#?}", id);
                    let r = match dbus::update_call(&conn, &ctx, &id, update).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e.into()),
                    };
                    match tx.send(r) {
                        Ok(_) => (),
                        Err(_) => error!("call update completion handler gone"),
                    }
                }
                Event::RemoveCall(id, tx) => {
                    trace!("[D-Bus] Event::RemoveCall {:#?}", id);
                    let r = match dbus::remove_call(&conn, &id).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e.into()),
                    };
                    match tx.send(r) {
                        Ok(_) => (),
                        Err(_) => error!("call removal completion handler gone"),
                    }
                }
                Event::UpdateNet(id, update, tx) => {
                    trace!("[D-Bus] Event::UpdateNet {:#?}", id);
                    let r = match dbus::update_net(&conn, &ctx, &id, &ports, update).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e.into()),
                    };
                    match tx.send(r) {
                        Ok(_) => (),
                        Err(_) => error!("network update completion handler gone"),
                    }
                }
                Event::RemoveNet(id, tx) => {
                    trace!("[D-Bus] Event::RemoveNet {:#?}", id);
                    let r = match dbus::remove_net(&conn, &id).await {
                        Ok(_) => Ok(()),
                        Err(e) => Err(e.into()),
                    };
                    match tx.send(r) {
                        Ok(_) => (),
                        Err(_) => error!("network removal completion handler gone"),
                    }
                }
                Event::NetPort(id, iface) => {
                    trace!("Event::NetPort {id:?} {iface}");
                    ports.insert(id, iface);
                }
            }
            trace!("Finished processing event");
        }
    });

    tokio::spawn(async move {
        while let Some((a, sender)) = stream.next().await {
            debug!("starting action {:#?}", a);
            let handle = qrtr.clone();
            tokio::spawn(async move {
                let result = handle.action(a).await;
                debug!("action completed");
                if let Some(tx) = sender {
                    match tx.send(result) {
                        Ok(_) => (),
                        Err(_) => error!("action completion handler gone"),
                    }
                }
            });
            trace!("Finished processing action");
        }
        error!("No more actions to process");
    });

    match devices::udev::try_monitor_blocking(&event_tx).await {
        Ok(_) => error!("Monitor task exited"),
        Err(e) => error!("Monitor task failed: {e}"),
    }
}
