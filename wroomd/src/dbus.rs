use crate::context::StateContext;
use crate::devices::{
    Action, CallID, CallStatus, CardStatus, CodeKind, DataConfigIpv4, DataConfigIpv6,
    DataNetworkID, DataNetworkStatus, PortID, SlotID, SlotStatus,
};
use log::{debug, info};
use std::collections::HashMap;
use zbus::{dbus_interface, Connection};

pub fn fmt_path(id: impl std::fmt::Display) -> String {
    format!("/de/abscue/obp/Wroomd/{id}")
}

pub struct Manager;

#[dbus_interface(name = "de.abscue.obp.Wroomd")]
impl Manager {
    #[dbus_interface(property)]
    fn version(&self) -> String {
        String::from(env!("CARGO_PKG_VERSION"))
    }
}

#[derive(Debug, zbus::DBusError)]
#[dbus_error(prefix = "de.abscue.obp.Wroomd")]
enum Error {
    #[dbus_error(zbus_error)]
    ZBus(zbus::Error),
    Internal(String),
    NoCard(String),
    AlreadyVerified(String),
    InvalidCode(String),
}

pub struct Slot {
    ctx: StateContext<()>,
    id: SlotID,
    status: SlotStatus,
}

fn parse_code_kind(code_kind: &str) -> Result<CodeKind, Error> {
    match code_kind {
        "pin1" => Ok(CodeKind::PIN1),
        "pin2" => Ok(CodeKind::PIN2),
        "upin" => Ok(CodeKind::UPIN),
        s => Err(Error::InvalidCode(format!(
            "value '{s}' invalid for code_kind"
        ))),
    }
}

impl Slot {
    fn check_unlock_ability(&self, ck: &CodeKind) -> Result<(), Error> {
        if let Some(card) = &self.status.card {
            if let Some(code) = card.codes.iter().find(|code| *ck == code.kind) {
                if code.verified {
                    Err(Error::AlreadyVerified(format!("{ck} already unlocked")))
                } else {
                    Ok(())
                }
            } else {
                Err(Error::InvalidCode(String::from("not supported by card")))
            }
        } else {
            Err(Error::NoCard(String::from("card not present")))
        }
    }
}

#[dbus_interface(name = "de.abscue.obp.Wroomd.Slot")]
impl Slot {
    async fn activate(&self) -> Result<(), Error> {
        self.ctx
            .action(Action::ActivateCard(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn deactivate(&self) -> Result<(), Error> {
        self.ctx
            .action(Action::DeactivateCard(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn unlock(&self, code_kind: String, code: String) -> Result<(), Error> {
        let ck = parse_code_kind(&code_kind)?;
        debug!("unlock {:#?} with {code}", ck);
        self.check_unlock_ability(&ck)?;
        self.ctx
            .action(Action::UnlockCard {
                slot: self.id.clone(),
                code_kind: ck,
                code,
            })
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn unblock(&self, code_kind: String, puk: String, new_code: String) -> Result<(), Error> {
        let ck = parse_code_kind(&code_kind)?;
        debug!("unblock {:#?} with puk {puk} and new code {new_code}", ck);
        self.check_unlock_ability(&ck)?;
        self.ctx
            .action(Action::UnblockCard {
                slot: self.id.clone(),
                code_kind: ck,
                puk,
                new_code,
            })
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn dial(&self, number: String) -> Result<(), Error> {
        debug!("dial {number}");
        self.ctx
            .action(Action::Dial {
                slot: self.id.clone(),
                number,
            })
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    #[dbus_interface(property)]
    fn card_present(&self) -> bool {
        self.status.card.is_some()
    }
    #[dbus_interface(property)]
    fn priority(&self) -> u32 {
        match &self.status.card {
            Some(CardStatus { priority, .. }) => *priority,
            None => 0,
        }
    }
    #[dbus_interface(property)]
    fn lock(&self) -> String {
        match &self.status.card {
            Some(CardStatus { lock, .. }) => lock.to_string(),
            None => String::from("none"),
        }
    }
    #[dbus_interface(property)]
    fn codes(&self) -> Vec<(String, (bool, u32, u32))> {
        match &self.status.card {
            Some(CardStatus { codes, .. }) => codes
                .iter()
                .map(|code| {
                    (
                        code.kind.to_string(),
                        (
                            code.verified,
                            code.retries as u32,
                            code.unblock_retries as u32,
                        ),
                    )
                })
                .collect(),
            None => vec![],
        }
    }
    #[dbus_interface(property)]
    fn signal(&self) -> u32 {
        self.status.network.signal as u32
    }
    #[dbus_interface(property)]
    fn network_state(&self) -> String {
        self.status.network.state.to_string()
    }
    #[dbus_interface(property)]
    fn network_technology(&self) -> String {
        self.status.network.technology.to_string()
    }
    #[dbus_interface(property)]
    fn network_operator(&self) -> String {
        self.status.network.operator.clone()
    }
    #[dbus_interface(property)]
    fn roaming(&self) -> bool {
        self.status.network.roaming
    }
}

pub async fn update_slot(
    conn: &Connection,
    ctx: &StateContext<()>,
    id: &SlotID,
    update: impl FnOnce(&SlotStatus) -> SlotStatus,
) -> zbus::Result<()> {
    let path = fmt_path(id);
    let srv = conn.object_server();
    match srv.interface::<_, Slot>(path.clone()).await {
        Ok(iface_ref) => {
            debug!("Found existing D-Bus interface for {path}");
            let mut iface = iface_ref.get_mut().await;
            let sc = iface_ref.signal_context();

            let new = update(&iface.status);

            let mut card_present_changed = false;
            let mut priority_changed = false;
            let mut lock_changed = false;
            let mut codes_changed = false;
            let mut signal_changed = false;
            let mut network_state_changed = false;
            let mut network_technology_changed = false;
            let mut network_operator_changed = false;
            let mut roaming_changed = false;

            if new.card.is_none() != iface.status.card.is_none() {
                card_present_changed = true;
                codes_changed = true;
            } else if let (Some(nc), Some(c)) = (&new.card, &iface.status.card) {
                if c.priority != nc.priority {
                    priority_changed = true;
                }
                if c.lock != nc.lock {
                    lock_changed = true;
                }
                if c.codes != nc.codes {
                    codes_changed = true;
                }
            }
            if new.network.signal != iface.status.network.signal {
                signal_changed = true;
            }
            if new.network.state != iface.status.network.state {
                network_state_changed = true;
            }
            if new.network.technology != iface.status.network.technology {
                network_technology_changed = true;
            }
            if new.network.operator != iface.status.network.operator {
                network_operator_changed = true;
            }
            if new.network.roaming != iface.status.network.roaming {
                roaming_changed = true;
            }

            iface.status = new;
            if card_present_changed {
                iface.card_present_changed(sc).await?;
            }
            if priority_changed {
                iface.priority_changed(sc).await?;
            }
            if lock_changed {
                iface.lock_changed(sc).await?;
            }
            if codes_changed {
                iface.codes_changed(sc).await?;
            }
            if signal_changed {
                iface.signal_changed(sc).await?;
            }
            if network_state_changed {
                iface.network_state_changed(sc).await?;
            }
            if network_technology_changed {
                iface.network_technology_changed(sc).await?;
            }
            if network_operator_changed {
                iface.network_operator_changed(sc).await?;
            }
            if roaming_changed {
                iface.roaming_changed(sc).await?;
            }
            Ok(())
        }
        Err(zbus::Error::InterfaceNotFound) => {
            info!("New slot at {path}");
            let default_status = SlotStatus::default();
            let status = update(&default_status);
            let iface = Slot {
                ctx: ctx.clone(),
                id: id.clone(),
                status,
            };
            srv.at(path, iface).await?;
            Ok(())
        }
        Err(err) => Err(err),
    }
}

pub struct Call {
    ctx: StateContext<()>,
    id: CallID,
    status: CallStatus,
}

#[dbus_interface(name = "de.abscue.obp.Wroomd.Call")]
impl Call {
    async fn accept(&self) -> Result<(), Error> {
        self.ctx
            .action(Action::Accept(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn hangup(&self) -> Result<(), Error> {
        self.ctx
            .action(Action::Hangup(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    #[dbus_interface(property)]
    fn number(&self) -> String {
        self.status.number.clone()
    }
    #[dbus_interface(property)]
    fn direction(&self) -> String {
        self.status.direction.to_string()
    }
    #[dbus_interface(property)]
    fn state(&self) -> String {
        self.status.state.to_string()
    }
}

pub async fn update_call(
    conn: &Connection,
    ctx: &StateContext<()>,
    id: &CallID,
    update: impl FnOnce(&CallStatus) -> CallStatus,
) -> zbus::Result<()> {
    let path = fmt_path(id);
    let srv = conn.object_server();
    match srv.interface::<_, Call>(path.clone()).await {
        Ok(iface_ref) => {
            debug!("Found existing D-Bus interface for {path}");
            let mut iface = iface_ref.get_mut().await;
            let sc = iface_ref.signal_context();

            let new = update(&iface.status);

            let mut number_changed = false;
            let mut direction_changed = false;
            let mut state_changed = false;

            if new.number != iface.status.number {
                number_changed = true;
            }
            if new.direction != iface.status.direction {
                direction_changed = true;
            }
            if new.state != iface.status.state {
                state_changed = true;
            }

            iface.status = new;
            if number_changed {
                iface.number_changed(sc).await?;
            }
            if direction_changed {
                iface.direction_changed(sc).await?;
            }
            if state_changed {
                iface.state_changed(sc).await?;
            }
            Ok(())
        }
        Err(zbus::Error::InterfaceNotFound) => {
            info!("New call at {path}");
            let default_status = CallStatus::default();
            let status = update(&default_status);
            let iface = Call {
                ctx: ctx.clone(),
                id: id.clone(),
                status,
            };
            srv.at(path, iface).await?;
            Ok(())
        }
        Err(err) => Err(err),
    }
}

pub async fn remove_call(conn: &Connection, id: &CallID) -> zbus::Result<()> {
    let path = fmt_path(id);
    let srv = conn.object_server();
    match srv.remove::<Call, _>(path.clone()).await {
        Ok(_) => info!("Removed call at {path}"),
        Err(zbus::Error::InterfaceNotFound) => debug!("Call at {path} does not exist"),
        Err(err) => return Err(err),
    }
    Ok(())
}

pub struct Network {
    ctx: StateContext<()>,
    id: DataNetworkID,
    status: DataNetworkStatus,
}

#[dbus_interface(name = "de.abscue.obp.Wroomd.Network")]
impl Network {
    async fn start(&self) -> Result<(), Error> {
        self.ctx
            .action(Action::StartNetwork(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))
    }
    async fn stop(&mut self) -> Result<(), Error> {
        self.ctx
            .action(Action::StopNetwork(self.id.clone()))
            .await
            .map_err(|err| Error::Internal(err.to_string()))?;
        // reset status
        self.status = DataNetworkStatus {
            apn: self.status.apn.clone(),
            ..Default::default()
        };
        Ok(())
    }
    #[dbus_interface(property)]
    fn apn(&self) -> String {
        self.status.apn.clone()
    }
    #[dbus_interface(property)]
    fn ipv4_state(&self) -> String {
        self.status.state_ipv4.to_string()
    }
    #[dbus_interface(property)]
    fn ipv6_state(&self) -> String {
        self.status.state_ipv6.to_string()
    }
    #[dbus_interface(property)]
    fn ipv4(&self) -> HashMap<String, String> {
        let DataConfigIpv4 { ifname, addr, gateway, dns, .. } = &self.status.config_ipv4;
        [
            ("Interface", ifname.clone()),
            ("Address", addr.map(|n| n.to_string())),
            ("Gateway", gateway.map(|a| a.to_string())),
            ("DNS", dns.map(|a| a.to_string())),
        ]
        .into_iter()
        .filter_map(|(key, option)| option.map(move |value| (key.into(), value)))
        .collect()
    }
    #[dbus_interface(property)]
    fn ipv6(&self) -> HashMap<String, String> {
        let DataConfigIpv6 { ifname, addr, gateway, dns, .. } = &self.status.config_ipv6;
        [
            ("Interface", ifname.clone()),
            ("Address", addr.map(|n| n.to_string())),
            ("Gateway", gateway.map(|a| a.to_string())),
            ("DNS", dns.map(|a| a.to_string())),
        ]
        .into_iter()
        .filter_map(|(key, option)| option.map(move |value| (key.into(), value)))
        .collect()
    }
}

pub async fn update_net(
    conn: &Connection,
    ctx: &StateContext<()>,
    id: &DataNetworkID,
    ports: &HashMap<PortID, String>,
    update: impl FnOnce(&DataNetworkStatus) -> DataNetworkStatus,
) -> zbus::Result<()> {
    let path = fmt_path(id);
    let srv = conn.object_server();
    match srv.interface::<_, Network>(path.clone()).await {
        Ok(iface_ref) => {
            debug!("Found existing D-Bus interface for {path}");
            let mut iface = iface_ref.get_mut().await;
            let sc = iface_ref.signal_context();

            let new = update(&iface.status);

            let mut apn_changed = false;
            let mut ipv4_state_changed = false;
            let mut ipv6_state_changed = false;
            let mut ipv4_changed = false;
            let mut ipv6_changed = false;

            if new.apn != iface.status.apn {
                apn_changed = true;
            }
            if new.state_ipv4 != iface.status.state_ipv4 {
                ipv4_state_changed = true;
            }
            if new.state_ipv6 != iface.status.state_ipv6 {
                ipv6_state_changed = true;
            }
            if new.config_ipv4 != iface.status.config_ipv4 {
                ipv4_changed = true;
            }
            if new.config_ipv6 != iface.status.config_ipv6 {
                ipv6_changed = true;
            }

            iface.status = new.resolve_ports(ports);
            if apn_changed {
                iface.apn_changed(sc).await?;
            }
            if ipv4_state_changed {
                iface.ipv4_state_changed(sc).await?;
            }
            if ipv6_state_changed {
                iface.ipv6_state_changed(sc).await?;
            }
            if ipv4_changed {
                iface.ipv4_changed(sc).await?;
            }
            if ipv6_changed {
                iface.ipv6_changed(sc).await?;
            }
            Ok(())
        }
        Err(zbus::Error::InterfaceNotFound) => {
            info!("New network at {path}");
            let default_status = DataNetworkStatus::default();
            let status = update(&default_status);
            let iface = Network {
                ctx: ctx.clone(),
                id: id.clone(),
                status,
            };
            srv.at(path, iface).await?;
            Ok(())
        }
        Err(err) => Err(err),
    }
}

pub async fn remove_net(conn: &Connection, id: &DataNetworkID) -> zbus::Result<()> {
    let path = fmt_path(id);
    let srv = conn.object_server();
    match srv.remove::<Network, _>(path.clone()).await {
        Ok(_) => info!("Removed network at {path}"),
        Err(zbus::Error::InterfaceNotFound) => debug!("Network at {path} does not exist"),
        Err(err) => return Err(err),
    }
    Ok(())
}
