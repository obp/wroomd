use crate::error::{Error, Result};
use std::collections::HashMap;

pub type TlvMap<'a> = HashMap<u8, &'a [u8]>;
pub type Tlv<'a> = (u8, &'a [u8]);

fn parse_tlv(bytes: &[u8]) -> Option<Result<(Tlv, &[u8])>> {
    if bytes.is_empty() {
        None
    } else if bytes.len() < 3 {
        Some(Err(Error::Eof))
    } else {
        let len = (bytes[2] as usize) << 8 | (bytes[1] as usize);
        let rest = &bytes[3..];
        if rest.len() < len {
            Some(Err(Error::Eof))
        } else {
            Some(Ok(((bytes[0], &rest[..len]), &rest[len..])))
        }
    }
}

fn entries(bytes: &[u8]) -> Result<Vec<Tlv>> {
    std::iter::successors(
        parse_tlv(bytes),
        |prev: &Result<(Tlv, &[u8])>| match &prev {
            Ok((_, next)) => parse_tlv(next),
            Err(_) => None,
        },
    )
    .map(|result| match result {
        Ok((tlv, _)) => Ok(tlv),
        Err(e) => Err(e),
    })
    .collect()
}

pub fn parse_tlvs(bytes: &[u8]) -> Result<TlvMap> {
    Ok(HashMap::from_iter(entries(bytes)?))
}

pub fn build_tlvs(entries: impl IntoIterator<Item = (u8, Vec<u8>)>) -> Result<Vec<u8>> {
    let tlvs = entries
        .into_iter()
        .flat_map(|(t, v)| {
            let l = v.len();
            vec![vec![t, l as u8, (l >> 8) as u8], v].concat()
        })
        .collect();
    Ok(tlvs)
}
