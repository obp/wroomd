use std::error::Error;
use std::future::pending;
use zbus::ConnectionBuilder;

// using multiple threads causes problems with QRTR sockets (they hang sometimes)
#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();

    let conn = ConnectionBuilder::system()?
        .name("de.abscue.obp.Wroomd")?
        .serve_at("/de/abscue/obp/Wroomd", wroomd::dbus::Manager {})?
        .serve_at("/de/abscue/obp/Wroomd", zbus::fdo::ObjectManager {})?
        .build()
        .await?;

    wroomd::state::track_state(conn).await;

    pending::<()>().await;

    Ok(())
}
