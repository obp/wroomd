static GSM_CHARSET: [char; 128] = [
    '@', '£', '$', '¥', 'è', 'é', 'ù', 'ì', 'ò', 'Ç', '\n', 'Ø', 'ø', '\r', 'Å', 'å', 'Δ', '_',
    'Φ', 'Γ', 'Λ', 'Ω', 'Π', 'Ψ', 'Σ', 'Θ', 'Ξ', '\x1b', 'Æ', 'æ', 'ß', 'É', ' ', '!', '"', '#',
    '¤', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6',
    '7', '8', '9', ':', ';', '<', '=', '>', '?', '¡', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
    'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'Ä', 'Ö',
    'Ñ', 'Ü', '§', '¿', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ä', 'ö', 'ñ', 'ü', 'à',
];

pub fn gsm8_decode(s: &[u8]) -> String {
    s.iter()
        .map(|&byte| GSM_CHARSET.get(byte as usize).copied().unwrap_or('�'))
        .scan(false, |is_esc, ch| {
            Some(if *is_esc {
                *is_esc = false;
                Some(match ch {
                    '\n' => '\x0c',
                    'Λ' => '^',
                    '(' => '{',
                    ')' => '}',
                    '/' => '\\',
                    '<' => '[',
                    '=' => '~',
                    '>' => ']',
                    '¡' => '|',
                    'e' => '€',
                    _ => '�',
                })
            } else if ch == '\x1b' {
                *is_esc = true;
                None
            } else {
                Some(ch)
            })
        })
        .flatten()
        .collect()
}

pub fn ucs2le_decode(s: &[u8]) -> String {
    let chars: Vec<u16> = s
        .chunks_exact(2)
        .map(|b| (b[1] as u16) << 8 | (b[0] as u16))
        .collect();
    String::from_utf16_lossy(&chars)
}
