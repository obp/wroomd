extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn::{Attribute, Data, DeriveInput, Expr, Ident, Meta, Path};

fn get_id_attr(attrs: &[Attribute]) -> &Expr {
    attrs
        .iter()
        .find_map(|a| match &a.meta {
            Meta::NameValue(nv) => {
                if nv.path.is_ident("id") {
                    Some(&nv.value)
                } else {
                    None
                }
            }
            _ => None,
        })
        .expect("Expected `id = `")
}

#[proc_macro_derive(QmiMessage, attributes(id, qmi_indication, qmi_request, qmi_response))]
pub fn derive_qmi_message(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as DeriveInput);

    let variants = match ast.data {
        Data::Enum(e) => e.variants,
        _ => panic!("QMI message trait can only be derived for enums"),
    };

    let msgtype: u8 = ast
        .attrs
        .iter()
        .find_map(|a| match &a.meta {
            Meta::Path(p) => {
                if p.is_ident("qmi_request") {
                    Some(0)
                } else if p.is_ident("qmi_response") {
                    Some(2)
                } else if p.is_ident("qmi_indication") {
                    Some(4)
                } else {
                    None
                }
            }
            _ => None,
        })
        .expect("Expected message type attribute");

    let idents: Vec<&Ident> = variants.iter().map(|v| &v.ident).collect();

    let msgids: Vec<&Expr> = variants.iter().map(|v| get_id_attr(&v.attrs)).collect();

    let structbuilders: Vec<_> = variants
        .iter()
        .map(|v| {
            let names = v.fields.iter().map(|f| f.ident.as_ref().unwrap());
            let field_ids = v.fields.iter().map(|f| get_id_attr(&f.attrs));
            quote! {
                #(
                    #names: match tlvs.get(&#field_ids) {
                        Some(bytes) => Some(qmi::struct_from_bytes(bytes)?),
                        None => None,
                    }
                ),*
            }
        })
        .collect();

    let fields: Vec<_> = variants
        .iter()
        .map(|v| {
            let names = v.fields.iter().map(|f| f.ident.as_ref().unwrap());
            quote! { #( #names ),* }
        })
        .collect();

    let tlvbuilders: Vec<_> = variants
        .iter()
        .map(|v| {
            let names = v.fields.iter().map(|f| f.ident.as_ref().unwrap());
            let field_ids = v.fields.iter().map(|f| get_id_attr(&f.attrs));
            let numfields = names.len();
            quote! {
                let entries: [Option<(u8, Vec<u8>)>; #numfields] = [
                    #(
                        match #names {
                            Some(field) => Some((#field_ids, field.to_bytes()?)),
                            None => None,
                        },
                    )*
                ];
                qmi::tlv::build_tlvs(entries.into_iter().flatten())?
            }
        })
        .collect();

    let name = &ast.ident;

    let code = quote! {
        impl QmiMessage for #name {
            fn from_bytes(bytes: &[u8]) -> qmi::Result<(u16, Self)> {
                let hdr: qmi::QmiHeader = qmi::struct_from_bytes(&bytes[..7])?;
                if hdr.ty != #msgtype {
                    Err(qmi::Error::UnexpectedType)
                } else {
                    let tlvs = qmi::tlv::parse_tlvs(&bytes[7..])?;
                    match hdr.msg_id {
                        #(
                            #msgids => {
                                Ok((hdr.txn_id, #name::#idents {
                                    #structbuilders
                                }))
                            }
                        ),*
                        _ => Err(qmi::Error::UnknownMsg)
                    }
                }
            }
            fn to_bytes(&self, txn_id: u16) -> qmi::Result<Vec<u8>> {
                let (msg_id, tlvs) = match self {
                    #(
                        #name::#idents { #fields } => (#msgids, {
                            #tlvbuilders
                        }),
                    )*
                };
                let len = u16::try_from(tlvs.len())
                    .map_err(|_| qmi::Error::SequenceTooLong)?;
                let hdr = qmi::QmiHeader {
                    ty: #msgtype,
                    txn_id: txn_id,
                    msg_id: msg_id,
                    msg_len: len,
                }.to_bytes()?;
                Ok([hdr, tlvs].concat())
            }
        }
    };

    code.into()
}

#[proc_macro_derive(QmiEnum)]
pub fn derive_qmi_enum(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as DeriveInput);
    let name = &ast.ident;

    let variants = match ast.data {
        Data::Enum(e) => e.variants,
        _ => panic!("QmiEnum needs an enum type"),
    };

    let repr: Path = ast
        .attrs
        .iter()
        .find_map(|a| a.parse_args().ok())
        .expect("QmiEnum needs a `repr(...)` attribute");

    let numbers: Vec<&Expr> = variants
        .iter()
        .map(|v| {
            v.discriminant
                .as_ref()
                .expect("QmiEnum requires explicit discriminants")
        })
        .map(|(_, disc)| disc)
        .collect();

    let idents: Vec<&Ident> = variants.iter().map(|v| &v.ident).collect();

    let code = quote! {
        impl QmiStruct for #name {
            fn from_bytes(bytes: &[u8]) -> qmi::Result<(Self, &[u8])> {
                let (v, rest): (#repr, &[u8]) = qmi::first_struct_from_bytes(bytes)?;
                match v {
                    #(
                        #numbers => Ok((#name::#idents, rest)),
                    )*
                    _ => Err(qmi::Error::UnknownEnum(v as u64)),
                }
            }
            fn to_bytes(&self) -> qmi::Result<Vec<u8>> {
                let num: #repr = match self {
                    #(
                        #name::#idents => #numbers,
                    )*
                };
                num.to_bytes()
            }
        }
    };

    code.into()
}

#[proc_macro_derive(QmiStruct)]
pub fn derive_qmi_struct(input: TokenStream) -> TokenStream {
    let ast = syn::parse_macro_input!(input as DeriveInput);

    let fields = match ast.data {
        Data::Struct(e) => e.fields,
        _ => panic!("QMI struct trait can only be derived for structs"),
    };

    let idents: Vec<&Ident> = fields
        .iter()
        .map(|v| v.ident.as_ref().expect("Named fields required"))
        .collect();

    let name = &ast.ident;

    let code = quote! {
        impl QmiStruct for #name {
            fn from_bytes(bytes: &[u8]) -> qmi::Result<(Self, &[u8])> {
                #(
                    let (#idents, bytes) = qmi::first_struct_from_bytes(bytes)?;
                )*
                Ok((#name {
                    #( #idents ),*
                }, bytes))
            }
            fn to_bytes(&self) -> qmi::Result<Vec<u8>> {
                Ok(vec![
                    #(
                        self.#idents.to_bytes()?,
                    )*
                ].into_iter().flatten().collect())
            }
        }
    };

    code.into()
}
