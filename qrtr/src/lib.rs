use futures::sink::Sink;
use futures::stream::BoxStream;
use libc::{self, c_int, sa_family_t, SOCK_CLOEXEC, SOCK_DGRAM, SOCK_NONBLOCK};
use log::trace;
use std::io::{Error, Result};
use std::pin::Pin;
use tokio::io::unix::AsyncFd;

const AF_QIPCRTR: c_int = 42;

#[derive(Debug)]
pub struct QrtrMessage {
    pub node: u32,
    pub port: u32,
    pub data: Vec<u8>,
}

#[repr(C)]
struct sockaddr_qrtr {
    sq_family: sa_family_t,
    sq_node: u32,
    sq_port: u32,
}

fn send(socket: c_int, qm: &QrtrMessage) -> Result<()> {
    let sq = sockaddr_qrtr {
        sq_family: AF_QIPCRTR as sa_family_t,
        sq_node: qm.node,
        sq_port: qm.port,
    };
    trace!("> calling sendto {}:{}", qm.node, qm.port);
    let ret = unsafe {
        libc::sendto(
            socket,
            qm.data.as_ptr() as *const libc::c_void,
            qm.data.len(),
            0,
            &sq as *const _ as *const libc::sockaddr,
            std::mem::size_of::<sockaddr_qrtr>() as libc::socklen_t,
        )
    };
    trace!("< sendto returned");
    if ret < 0 {
        Err(Error::last_os_error())
    } else {
        Ok(())
    }
}

fn recv(socket: c_int) -> Result<QrtrMessage> {
    unsafe {
        let mut sq: sockaddr_qrtr = std::mem::MaybeUninit::zeroed().assume_init();
        let mut sq_len = std::mem::size_of::<sockaddr_qrtr>() as libc::socklen_t;
        let mut data: [u8; 1024] = std::mem::MaybeUninit::zeroed().assume_init();
        let ret = libc::recvfrom(
            socket,
            data.as_mut_ptr() as *mut libc::c_void,
            data.len(),
            0,
            &mut sq as *mut _ as *mut libc::sockaddr,
            &mut sq_len as *mut _,
        );
        if ret < 0 {
            Err(Error::last_os_error())
        } else {
            Ok(QrtrMessage {
                node: sq.sq_node,
                port: sq.sq_port,
                data: Vec::from(&data[..(ret as usize)]),
            })
        }
    }
}

fn open_socket() -> Result<c_int> {
    unsafe {
        let ret = libc::socket(AF_QIPCRTR, SOCK_DGRAM | SOCK_NONBLOCK | SOCK_CLOEXEC, 0);
        if ret < 0 {
            Err(Error::last_os_error())
        } else {
            Ok(ret)
        }
    }
}

pub struct QrtrSocket {
    fd: AsyncFd<c_int>,
}

impl QrtrSocket {
    pub fn new() -> Result<Self> {
        let fd = open_socket()?;
        Ok(QrtrSocket {
            fd: AsyncFd::new(fd)?,
        })
    }
    pub fn sink(&self) -> Pin<Box<dyn Sink<QrtrMessage, Error = Error> + Send + '_>> {
        Box::pin(futures::sink::unfold(0, |_, msg| async {
            self.write(msg).await?;
            Ok(0)
        }))
    }
    pub async fn write(&self, msg: QrtrMessage) -> Result<()> {
        let fd = *self.fd.get_ref();
        loop {
            trace!("Waiting for {fd} to become writable");
            let mut guard = self.fd.writable().await?;
            trace!("Writing...");
            match guard.try_io(|fd| send(*fd.get_ref(), &msg)) {
                Ok(result) => return result,
                Err(_) => trace!("Retrying write..."),
            }
        }
    }
    pub fn stream(&self) -> BoxStream<'_, Result<QrtrMessage>> {
        Box::pin(futures::stream::unfold(0, |_| async {
            Some((self.read().await, 0))
        }))
    }
    pub async fn read(&self) -> Result<QrtrMessage> {
        let fd = *self.fd.get_ref();
        loop {
            trace!("Waiting for {fd} to become readable");
            let mut guard = self.fd.readable().await?;
            trace!("Reading...");
            match guard.try_io(|fd| recv(*fd.get_ref())) {
                Ok(result) => return result,
                Err(_) => trace!("Retrying read..."),
            }
        }
    }
}

impl Drop for QrtrSocket {
    fn drop(&mut self) {
        let fd = *self.fd.get_ref();
        trace!("Closing {fd}");
        unsafe {
            libc::close(fd);
        }
    }
}
