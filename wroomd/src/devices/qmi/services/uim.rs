use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct};

pub const EVENT_CARD_STATUS: u32 = 1 << 0;
pub const EVENT_SAP_CONNECTION: u32 = 1 << 1;
pub const EVENT_EXTENDED_CARD_STATUS: u32 = 1 << 2;
pub const EVENT_PHYSICAL_SLOT_STATUS: u32 = 1 << 4;

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CardState {
    Absent = 0,
    Present = 1,
    Error = 2,
    Unknown = 3,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum CardError {
    Unknown = 0,
    PowerDown = 1,
    PollError = 2,
    NoATRReceived = 3,
    VoltMismatch = 4,
    ParityError = 5,
    PossiblyRemoved = 6,
    TechnicalProblems = 7,
    NullBytes = 8,
    SAPConnected = 9,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum PinId {
    Unknown = 0,
    PIN1 = 1,
    PIN2 = 2,
    UPIN = 3,
    HiddenKey = 4,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum PinState {
    Unknown = 0,
    Unverified = 1,
    Verified = 2,
    Disabled = 3,
    Blocked = 4,
    PermanentlyBlocked = 5,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum MainPinType {
    PIN1 = 0,
    UPIN = 1,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum AppType {
    Unknown = 0,
    SIM = 1,
    USIM = 2,
    RUIM = 3,
    CSIM = 4,
    ISIM = 5,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum AppState {
    Unknown = 0,
    Detected = 1,
    PinRequired = 2,
    PukRequired = 3,
    PersoRequired = 4,
    PermanentlyBlocked = 5,
    Illegal = 6,
    Ready = 7,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum PersoState {
    Unknown = 0,
    InProgress = 1,
    Ready = 2,
    CodeRequired = 3,
    PukRequired = 4,
    PermanentlyBlocked = 5,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum PersoFeatures {
    // GW
    Network = 0,
    NetworkSubset = 1,
    ServiceProviderGw = 2,
    CorporateGw = 3,
    UIM = 4,
    // 1X
    NetworkType1 = 5,
    NetworkType2 = 6,
    HPRD = 7,
    ServiceProvider1x = 8,
    Corporate1x = 9,
    RUIM = 10,

    Unknown = 11,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum SessionType {
    PrimaryGwProvisioning = 0,
    Primary1xProvisioning = 1,
    SecondaryGwProvisioning = 2,
    Secondary1xProvisioning = 3,
    NonprovisioningSlot1 = 4,
    NonprovisioningSlot2 = 5,
    CardSlot1 = 6,
    CardSlot2 = 7,
    LogicalChannelSlot1 = 8,
    LogicalChannelSlot2 = 9,
    TertiaryGwProvisioning = 10,
    Tertiary1xProvisioning = 11,
    QuaternaryGwProvisioning = 12,
    Quaternary1xProvisioning = 13,
    QuinaryGwProvisioning = 14,
    Quinary1xProvisioning = 15,
    NonprovisioningSlot3 = 16,
    NonprovisioningSlot4 = 17,
    NonprovisioningSlot5 = 18,
    CardSlot3 = 19,
    CardSlot4 = 20,
    CardSlot5 = 21,
    LogicalChannelSlot3 = 22,
    LogicalChannelSlot4 = 23,
    LogicalChannelSlot5 = 24,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct PinInfo {
    pub state: PinState,
    pub retries: u8,
    pub puk_retries: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct AppInfo {
    pub ty: AppType,
    pub state: AppState,
    pub perso_state: PersoState,
    pub perso_features: PersoFeatures,
    pub perso_retries: u8,
    pub perso_unblock_retries: u8,
    pub aid: Vec<u8>,
    pub main_pin: MainPinType,
    pub pin1: PinInfo,
    pub pin2: PinInfo,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct AppIndex {
    pub application: u8,
    pub slot: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct AppProvisioningInfo {
    pub slot: u8,
    pub aid: Vec<u8>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct CardInfo {
    pub state: CardState,
    pub upin: PinInfo,
    pub error: CardError,
    pub applications: Vec<AppInfo>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct CardStatus {
    pub index_gw: Vec<AppIndex>,
    pub index_1x: Vec<AppIndex>,
    pub cards: Vec<CardInfo>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct SessionInfo {
    pub session: SessionType,
    pub aid: Vec<u8>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct SessionChange {
    pub session: SessionType,
    pub activate: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct VerifyPinInfo {
    pub id: PinId,
    pub value: String,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct UnblockPinInfo {
    pub id: PinId,
    pub puk: String,
    pub new_pin: String,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct PinRetriesLeft {
    pub retries: u8,
    pub unblock_retries: u8,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x26]
    VerifyPin {
        #[id = 0x01]
        session: Option<SessionInfo>,
        #[id = 0x02]
        pin: Option<VerifyPinInfo>,
    },
    #[id = 0x27]
    UnblockPin {
        #[id = 0x01]
        session: Option<SessionInfo>,
        #[id = 0x02]
        pin: Option<UnblockPinInfo>,
    },
    #[id = 0x2E]
    RegisterEvents {
        #[id = 0x01]
        mask: Option<u32>,
    },
    #[id = 0x2F]
    GetCardStatus {
        #[id = 0x10]
        extended: Option<u8>,
    },
    #[id = 0x38]
    ChangeProvisioningSession {
        #[id = 0x01]
        session_change: Option<SessionChange>,
        #[id = 0x10]
        application: Option<AppProvisioningInfo>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x26]
    VerifyPin {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x10]
        retries_left: Option<PinRetriesLeft>,
    },
    #[id = 0x27]
    UnblockPin {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x10]
        retries_left: Option<PinRetriesLeft>,
    },
    #[id = 0x2E]
    RegisterEvents {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x2F]
    GetCardStatus {
        #[id = 0x02]
        result: Option<QmiResult>,
        #[id = 0x13]
        card_status: Option<CardStatus>,
    },
    #[id = 0x38]
    ChangeProvisioningSession {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_indication]
pub enum Ind {
    #[id = 0x32]
    CardStatus {
        #[id = 0x12]
        card_status: Option<CardStatus>,
    },
}
