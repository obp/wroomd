use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct};

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum RoamingIndicator {
    On = 0,
    Off = 1,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum DataCapability {
    None = 0,
    GPRS = 1,
    EDGE = 2,
    HSDPA = 3,
    HSUPA = 4,
    WCDMA = 5,
    CDMA = 6,
    EVDORev0 = 7,
    EVDORevA = 8,
    GSM = 9,
    EVDORevB = 10,
    LTE = 11,
    HSDPAPlus = 12,
    DCHSDPAPlus = 13,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum RegistrationState {
    NotRegistered = 0,
    Registered = 1,
    Searching = 2,
    Denied = 3,
    Unknown = 4,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum AttachState {
    Unknown = 0,
    Attached = 1,
    Detached = 2,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum Network {
    Unknown = 0,
    _3GPP2 = 1,
    _3GPP = 2,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum Encoding {
    GSM8 = 0,
    UCS2LE = 1,
}

#[derive(Clone, Debug, QmiEnum)]
#[repr(i8)]
pub enum RadioInterface {
    Unknown = -1,
    None = 0,
    Cdma1x = 1,
    Cdma1xEVDO = 2,
    AMPS = 3,
    GSM = 4,
    UMTS = 5,
    LTE = 8,
    TDSCDMA = 9,
    _5GNR = 12,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct NetworkRejectIndicationInfo {
    pub enable: u8,
    pub suppress_system_info: u8,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct PLMNInfo {
    pub mcc: u16,
    pub mnc: u16,
    pub name: Vec<u8>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct ServiceProviderName {
    pub encoding: Encoding,
    pub name: Vec<u8>,
}

#[derive(Clone, Debug, QmiStruct)]
pub struct ServingSystem {
    pub registration: RegistrationState,
    pub cs_attach: AttachState,
    pub ps_attach: AttachState,
    pub selected_network: Network,
    pub radio_interfaces: Vec<RadioInterface>,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x10]
        system_selection_preference: Option<u8>,
        #[id = 0x12]
        ddtm_events: Option<u8>,
        #[id = 0x13]
        serving_system_events: Option<u8>,
        #[id = 0x14]
        dual_standby_preference: Option<u8>,
        #[id = 0x15]
        subscription_info: Option<u8>,
        #[id = 0x17]
        network_time: Option<u8>,
        #[id = 0x18]
        system_info: Option<u8>,
        #[id = 0x19]
        signal_info: Option<u8>,
        #[id = 0x1A]
        error_rate: Option<u8>,
        #[id = 0x1B]
        hdr_new_uati_assigned: Option<u8>,
        #[id = 0x1C]
        hdr_session_closed: Option<u8>,
        #[id = 0x1D]
        managed_roaming: Option<u8>,
        #[id = 0x1E]
        current_plmn_name: Option<u8>,
        #[id = 0x1F]
        embms_status: Option<u8>,
        #[id = 0x20]
        rf_band_info: Option<u8>,
        #[id = 0x21]
        network_reject_info: Option<NetworkRejectIndicationInfo>,
    },
    #[id = 0x45]
    BindSubscription {
        #[id = 0x01]
        subscription: Option<super::Subscription>,
    },
    #[id = 0x4B]
    SetDualStandbyPreference {
        #[id = 0x12]
        data_subscription: Option<super::Subscription>,
    },
    #[id = 0x75]
    ConfigurePLMNNameReporting {
        #[id = 0x01]
        all_names: Option<u8>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x03]
    RegisterIndications {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x45]
    BindSubscription {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x4B]
    SetDualStandbyPreference {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
    #[id = 0x75]
    ConfigurePLMNNameReporting {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_indication]
pub enum Ind {
    #[id = 0x24]
    ServingSystem {
        #[id = 0x01]
        serving_system: Option<ServingSystem>,
        #[id = 0x10]
        roaming_indicator: Option<RoamingIndicator>,
        #[id = 0x11]
        data_service_cap: Option<Vec<DataCapability>>,
        #[id = 0x12]
        current_plmn: Option<PLMNInfo>,
    },
    #[id = 0x61]
    CurrentPLMNName {
        #[id = 0x11]
        spn: Option<ServiceProviderName>,
    },
}
