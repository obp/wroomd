use crate::context::StateContext;
use crate::devices::{ActionError, DeviceHandle};
use futures::future::BoxFuture;
use std::collections::HashMap;

mod common;
pub mod qrtr;
pub mod services;

use services::QmiService;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Client {
    Subs(u8),
    Net(u8, u8),
    Net6(u8, u8),
}

#[derive(Default)]
struct State {
    aids: HashMap<u8, Vec<u8>>,
    ports: HashMap<(u8, u8), u8>,
    last_used_port: u8,
    session_slots: Vec<Option<u8>>,
    slot_sessions: Vec<Option<u8>>,
    txn_id: std::num::Wrapping<u16>,
}

trait QmiDevice: DeviceHandle {
    fn close(&self, cid: Client) -> BoxFuture<'_, ()>;
    fn send_msg(
        &self,
        cid: Client,
        service: QmiService,
        data: Vec<u8>,
    ) -> BoxFuture<'_, Result<(), ActionError>>;
    fn state(&self) -> &'_ StateContext<State>;
}
