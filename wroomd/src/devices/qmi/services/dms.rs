use qmi::{QmiEnum, QmiMessage, QmiResult, QmiStruct};

#[derive(Clone, Debug, QmiEnum)]
#[repr(u8)]
pub enum OperatingMode {
    Online = 0,
    LowPower = 1,
    FactoryTest = 2,
    Offline = 3,
    Reset = 4,
    ShuttingDown = 5,
    PersistentLowPower = 6,
    ModeOnlyLowPower = 7,
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_request]
pub enum Req {
    #[id = 0x2E]
    SetOperatingMode {
        #[id = 0x01]
        mode: Option<OperatingMode>,
    },
}

#[derive(Clone, Debug, QmiMessage)]
#[qmi_response]
pub enum Resp {
    #[id = 0x2E]
    SetOperatingMode {
        #[id = 0x02]
        result: Option<QmiResult>,
    },
}
